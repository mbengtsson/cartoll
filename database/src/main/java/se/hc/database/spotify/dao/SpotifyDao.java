package se.hc.database.spotify.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;

import se.hc.database.spotify.Album;
import se.hc.database.spotify.Artist;

/**
 * A repository for Spotify, storing artists and albums This class is tested by
 * SpotifyDaoTest. Run the test and implements all the needed logic.
 */
public class SpotifyDao {
	Logger log = Logger.getLogger(SpotifyDao.class);

	/**
	 * Returns a Connection to a database.
	 */
	protected Connection getConnection() throws InstantiationException, IllegalAccessException, ClassNotFoundException,
			SQLException {

		String username = "root";
		String password = "pass123";
		String servername = "localhost";
		String schemaName = "test";
		int port = 3306;
		String connectionUrl = "jdbc:mysql://" + servername + ":" + port + "/" + schemaName;
		Connection conn = null;

		Properties connectionProps = new Properties();
		connectionProps.put("user", username);
		connectionProps.put("password", password);
		conn = DriverManager.getConnection(connectionUrl, connectionProps);

		return conn;

	}

	public void init(Set<Artist> artists) throws Exception {

		// Clear all tables
		dropTable("ARTISTS");
		dropTable("ALBUMS");

		// Create tables
		String artistSql =
				"CREATE TABLE ARTISTS (ID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, NAME VARCHAR(50), UNIQUE(NAME))";
		executeSql(artistSql);
		String albumSql =
				"CREATE TABLE ALBUMS (ID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY, NAME VARCHAR(50) UNIQUE, RELEASEYEAR YEAR, ARTIST_ID INTEGER)";
		executeSql(albumSql);

		// Insert all artists into the database. First, only insert artists.
		// Later we will store albums for each artist.
		for (Artist artist : artists) {
			createArtist(artist.getId(), artist);
		}

	}

	public Artist getArtist(long artistId) throws Exception {

		ResultSetHandler handler = new ResultSetHandler() {

			@Override
			public Object handleResultSet(ResultSet rs) throws Exception {

				Artist artist = null;
				while (rs.next()) {
					artist = new Artist(rs.getInt("ID"), rs.getString("NAME"));
				}

				return artist;
			}
		};

		String sql = "SELECT * FROM ARTISTS WHERE ID =" + artistId;
		query(sql, handler);

		Artist artist = (Artist) handler.getResult();

		// Get albums
		if (artist != null) {
			Set<Album> albums = getAlbumsForArtist(artist);
			artist.setAlbums(albums);
			for (Album album : albums) {
				album.setArtist(artist);
			}
		}

		return artist;

	}

	public Set<Album> getAlbumsForArtist(Artist artist) throws Exception {

		// Implent ResultSetHandler
		ResultSetHandler handler = new ResultSetHandler() {

			@Override
			public Object handleResultSet(ResultSet rs) throws Exception {
				HashSet<Album> albums = new HashSet<Album>();
				while (rs.next()) {
					int id = rs.getInt("ID");
					String name = rs.getString("NAME");
					int year = rs.getInt("RELEASEYEAR");
					Album album = new Album(id, name, year);
					albums.add(album);
				}

				return albums;

			}
		};

		// SQL
		String sql = "SELECT * FROM ALBUMS WHERE ARTIST_ID = " + artist.getId();
		query(sql, handler);
		Set<Album> albums = (Set<Album>) handler.getResult();

		return albums;

	}

	public void updateArtist(Artist artist) throws Exception {
		String sql = "UPDATE ARTISTS SET NAME = \"" + artist.getName() + "\" WHERE ID = " + artist.getId();
		executeSql(sql);
	}

	public void deleteArtist(long artistId) throws Exception {
		String sql = "DELETE FROM ARTISTS WHERE ID = " + artistId;
		executeSql(sql);
	}

	public int getArtistsSize() throws Exception {

		ResultSetHandler handler = new ResultSetHandler() {

			@Override
			public Object handleResultSet(ResultSet rs) throws Exception {

				int size = 0;
				while (rs.next()) {
					size = rs.getInt("numArtists");
				}

				return size;
			}
		};

		String sql = "SELECT count(*) AS numArtists FROM ARTISTS";
		query(sql, handler);

		return (Integer) handler.getResult();
	}

	public long createArtist(Artist artist) throws Exception {
		String sql = "INSERT INTO ARTISTS (NAME) VALUES (\"" + artist.getName() + "\")";
		executeSql(sql);
		long id = getMaxArtistId();

		for (Album album : artist.getAlbums()) {
			createAlbum(album);
		}

		return id;
	}

	public long createArtist(long id, Artist artist) throws Exception {
		String sql = "INSERT INTO ARTISTS (ID, NAME) VALUES(" + id + ", \"" + artist.getName() + "\")";
		executeSql(sql);

		for (Album album : artist.getAlbums()) {
			createAlbum(album);
		}

		return id;
	}

	public long createAlbum(Album album) throws Exception {

		executeSql("INSERT INTO ALBUMS (ID, NAME, ARTIST_ID, RELEASEYEAR) VALUES (" + album.getId() + ", '"
				+ album.getName() + "', " + album.getArtist().getId() + ", " + album.getReleaseYear() + ")");
		return album.getId();

	}

	/**
	 * Probably not a typical method, but represents the concept of updating
	 * multiple values in one transaction. If any update fails the whole
	 * transaction should be aborted.
	 * 
	 * Try using a PreparedStatement instead of a normal Statement
	 * 
	 */
	public void updateArtistNameAndAlbumName(Artist artist, String newArtistName, Album album, String newAlbumName)
			throws Exception {
		Connection conn = getConnection();
		String artistSql = "UPDATE ARTISTS SET NAME=? WHERE ID=?";
		String albumSql = "UPDATE ALBUMS SET NAME=? WHERE ID=?";
		PreparedStatement artistStmt = conn.prepareStatement(artistSql);
		PreparedStatement albumStmt = conn.prepareStatement(albumSql);
		conn.setAutoCommit(false);

		try {
			artistStmt.setString(1, newArtistName);
			artistStmt.setLong(2, artist.getId());
			artistStmt.executeUpdate();

			albumStmt.setString(1, newAlbumName);
			albumStmt.setLong(2, album.getId());
			albumStmt.executeUpdate();

			conn.commit();

		} catch (SQLException e) {
			conn.rollback();
			throw e;

		} finally {
			artistStmt.close();
			albumStmt.close();
			conn.close();
		}

	}

	protected void dropTable(String tableName) {
		try {
			executeSql("DROP TABLE " + tableName);
		} catch (Exception e) {
			log.debug("Could not drop table " + tableName + ". It has probably not been created yet");
		}

	}

	protected void executeSql(String sql) throws Exception {
		log.debug("Executing sql: " + sql);
		Connection con = getConnection();
		Statement stmt = con.createStatement();
		stmt.execute(sql);
		stmt.close();
		con.close();

	}

	private void query(String sql, ResultSetHandler handler) throws Exception {
		Connection con = getConnection();
		Statement stmt = con.createStatement();
		ResultSet rs = null;

		try {
			rs = stmt.executeQuery(sql);
			handler.handleResultSetInternal(rs);

		} finally {
			rs.close();
			stmt.close();
			con.close();
		}

	}

	/**
	 * Gets the highest ID for the Artists table
	 */
	public long getMaxArtistId() throws Exception {

		ResultSetHandler handler = new ResultSetHandler() {

			public Object handleResultSet(ResultSet rs) throws Exception {
				while (rs.next()) {
					int maxId = rs.getInt(1);
					return new Long(maxId);
				}
				return 0;
			}
		};

		query("select max(ID) from ARTISTS", handler);
		return (Long) handler.getResult();

	}

	/**
	 * Class that handles the logic of parsing the ResultSet. Different usecases
	 * will use a different parsing logic.
	 * 
	 */
	protected abstract class ResultSetHandler {

		private Object result;

		/**
		 * Called by the DAO. Simply stores the result in a variable for later
		 * retrieval.
		 */
		public void handleResultSetInternal(ResultSet rs) throws Exception {
			this.result = handleResultSet(rs);
		}

		/**
		 * The method that must be implemented by each usecase.
		 */
		public abstract Object handleResultSet(ResultSet rs) throws Exception;

		public Object getResult() {
			return result;
		}
	}

}
