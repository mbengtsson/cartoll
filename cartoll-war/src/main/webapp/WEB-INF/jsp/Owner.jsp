<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visa ägare</title>
<link href="<%=request.getContextPath()%>/style/style.css"
	type="text/css" rel="stylesheet" />
</head>
<body>
	<h1>Ägare: ${owner.fullName}</h1>
	<p>
		${owner.street} <br>
		${owner.zipCode} ${owner.city}
	</p>
	
	<table>
		<tr>
			<th>Bil</th>
			<th>Fordonstyp</th>
			<th>Antal passager</th>
			<th>Kostnad</th>
		</tr>
		<c:forEach var="vehicleStat" items="${vehicleStatistics}">
			<tr>
				<td>${vehicleStat.vehicle.regNr}</td>
				<td>${vehicleStat.vehicle.niceType}</td>
				<td>${vehicleStat.statistic.passages} st</td>
				<td>${vehicleStat.statistic.revenue} kr</td>
			</tr>
		</c:forEach>
	</table>
	<p><a href="${header.referer}">Tillbaka</a></p>
</body>
</html>