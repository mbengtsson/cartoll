<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lägg till passering</title>
<link href="<%=request.getContextPath()%>/style/style.css"
	type="text/css" rel="stylesheet" />
</head>
<body>
	<h1>Lägg till passering</h1>
	
	<form:form commandName="bean">
		<table class="vertical">
			<tr>
				<th>Fordon</th>
				<td>
					<form:select path="vehicleId">
						<form:options items="${vehicles}" itemValue="id" itemLabel="niceName"/>
					</form:select>
				</td>
			</tr>
			<tr>
				<th>Station</th>
				<td>
					<form:select path="stationId">
						<form:options items="${stations}" itemValue="id" itemLabel="name"/>
					</form:select>
				</td>
			</tr>
			<tr>
				<th>Tid (h/min)</th>
				<td>
					<form:select path="hour">
						<c:forEach var="i" begin="0" end="23">
							<form:option value="${i}" label="${i}"></form:option>
						</c:forEach>
					</form:select>
					<form:select path="minute">
						<c:forEach var="i" begin="0" end="59">
							<form:option value="${i}" label="${i}"></form:option>
						</c:forEach>
					</form:select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Lägg till" /></td>
			</tr>
		</table>
	</form:form>

	<p><a href='<c:url value="/index.html" />'>Tillbaka</a></p>
</body>
</html>