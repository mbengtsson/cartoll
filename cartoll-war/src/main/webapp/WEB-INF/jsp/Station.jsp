<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Visa station</title>
<link href="<%=request.getContextPath()%>/style/style.css"
	type="text/css" rel="stylesheet" />
</head>
<body>
	<h1>Station: ${station.name}</h1>
	<table>
		<tr>
			<th>Reg. Nr</th>
			<th>Fordonstyp</th>
			<th>Ägare</th>
			<th>Intäkt</th>
			<th>Datum</th>
		</tr>
		<c:forEach var="passage" items="${passages}">
			<tr>
				<td>${passage.vehicle.regNr}</td>
				<td>${passage.vehicle.niceType}</td>
				<td><a href='<c:url value="/owner/${passage.vehicle.owner.id}.html" />'>${passage.vehicle.owner.fullName}, ${passage.vehicle.owner.city}</a></td>
				<td>${passage.fee} kr</td>
				<td>${passage.niceTime}</td>	
			</tr>
		</c:forEach>
	</table>
	<p><a href='<c:url value="/index.html" />'>Tillbaka</a></p>
</body>
</html>