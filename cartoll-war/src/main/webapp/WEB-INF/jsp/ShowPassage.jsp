<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Passering tillagd</title>
<link href="<%=request.getContextPath()%>/style/style.css"
	type="text/css" rel="stylesheet" />
</head>
<body>
	<h1>Passering tillagd</h1>
	
	<table class="vertical">
		<tr>
			<th>ID</th>
			<td>${passage.id}</td>
		</tr>
		<tr>
			<th>Fordon</th>
			<td>${passage.vehicle.niceName}</td>
		</tr>
		<tr>
			<th>Station</th>
			<td>${passage.station.name}</td>
		</tr>
		<tr>
			<th>Tid</th>
			<td>${passage.niceTime}</td>
		</tr>
		<tr>
			<th>Kostnad</th>
			<td>${passage.fee} kr</td>
		</tr>
	</table>
	
	<p><a href='<c:url value="/index.html" />'>Startsidan</a> <a href='<c:url value="/addpassage.html" />'>Lägg till ny passering</a></p>
	
</body>
</html>