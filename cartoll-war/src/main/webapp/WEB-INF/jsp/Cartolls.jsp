<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Biltullarna</title>
<link href="<%=request.getContextPath()%>/style/style.css"
	type="text/css" rel="stylesheet" />
</head>
<body>
	<h1>Välkommen till biltullarna</h1>
	<p><a href='<c:url value="/addpassage.html" />'>Lägg till passering</a> <a href='<c:url value="/remove.html" />'>Rensa passeringar</a></p>
	<p>Totalt antal passager: ${totalStatistic.passages}<br>
	Total intäkt: ${totalStatistic.revenue} kr</p>

	<table>
		<tr>
			<th>Station</th>
			<th>Antal passager</th>
			<th>Intäkt</th>
		</tr>
		<c:forEach var="stationStat" items="${stationStatistics}">
			<tr>
				<td><a href='<c:url value="/station/${stationStat.station.id}.html" />'>${stationStat.station.name}</a></td>
				<td>${stationStat.statistic.passages} st</td>
				<td>${stationStat.statistic.revenue} kr</td>
			</tr>	
			
		</c:forEach>
	
	
	</table>
</body>
</html>