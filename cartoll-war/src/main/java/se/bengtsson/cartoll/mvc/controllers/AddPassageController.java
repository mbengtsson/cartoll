package se.bengtsson.cartoll.mvc.controllers;

import java.util.Collection;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;
import se.bengtsson.cartoll.mvc.beans.AddPassageBean;
import se.bengtsson.cartoll.services.PassageService;
import se.bengtsson.cartoll.services.StationService;
import se.bengtsson.cartoll.services.VehicleService;

@Controller
@RequestMapping("addpassage.html")
public class AddPassageController {

	@Autowired
	private StationService stationService;
	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private PassageService passageService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView addPassage() {

		Collection<Station> stations = stationService.getAllStations();
		Collection<Vehicle> vehicles = vehicleService.getAllVehicles();

		AddPassageBean addPassageBean = new AddPassageBean();

		ModelAndView mav = new ModelAndView("AddPassage");
		mav.addObject("stations", stations);
		mav.addObject("vehicles", vehicles);
		mav.addObject("bean", addPassageBean);

		return mav;
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView handleSubmit(AddPassageBean addPassageBean) {

		Vehicle vehicle = vehicleService.getVehicle(addPassageBean.getVehicleId());
		Station station = stationService.getStation(addPassageBean.getStationId());
		DateTime time = new LocalTime(addPassageBean.getHour(), addPassageBean.getMinute()).toDateTimeToday();

		Passage passage = passageService.createPassage(new Passage(station, vehicle, time));

		return new ModelAndView("redirect:/showpassage/" + passage.getId() + ".html");
	}
}
