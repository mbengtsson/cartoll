package se.bengtsson.cartoll.mvc.controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.services.PassageService;
import se.bengtsson.cartoll.services.StationService;

@Controller
public class StationController {

	@Autowired
	private StationService stationService;
	@Autowired
	private PassageService passageService;

	@RequestMapping("/station/{stationId}")
	public ModelAndView station(@PathVariable long stationId) {

		Station station = stationService.getStation(stationId);
		Collection<Passage> passages = passageService.getPassagesByStation(stationId);

		ModelAndView mav = new ModelAndView("Station");
		mav.addObject("station", station);
		mav.addObject("passages", passages);

		return mav;
	}

}
