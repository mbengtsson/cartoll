package se.bengtsson.cartoll.mvc.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.statistic.Statistic;
import se.bengtsson.cartoll.services.PassageService;
import se.bengtsson.cartoll.services.StationService;

@Controller
public class CartollsController {

	@Autowired
	private StationService stationService;
	@Autowired
	private PassageService passageService;

	@RequestMapping("/index.html")
	public ModelAndView cartolls() {

		Collection<Station> stations = stationService.getAllStations();
		List<StationAndStatisticWrapper> stationStatistics = new ArrayList<>();

		for (Station station : stations) {
			stationStatistics.add(new StationAndStatisticWrapper(station, passageService
					.getStatisticsByStation(station)));

		}

		ModelAndView mav = new ModelAndView("Cartolls");
		mav.addObject("stationStatistics", stationStatistics);
		mav.addObject("totalStatistic", passageService.getTotalStatistics());

		return mav;

	}

	@RequestMapping("/remove.html")
	public ModelAndView removePassages() {
		passageService.removeAllPassages();
		return new ModelAndView("redirect:/index.html");
	}

	public class StationAndStatisticWrapper {

		private Station station;
		private Statistic statistic;

		public StationAndStatisticWrapper(Station station, Statistic statistic) {
			setStation(station);
			setStatistic(statistic);

		}

		public Station getStation() {
			return station;
		}

		public Statistic getStatistic() {
			return statistic;
		}

		public void setStation(Station station) {
			this.station = station;
		}

		public void setStatistic(Statistic statistic) {
			this.statistic = statistic;
		}

	}

}
