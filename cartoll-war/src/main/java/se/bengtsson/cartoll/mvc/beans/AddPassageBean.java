package se.bengtsson.cartoll.mvc.beans;

public class AddPassageBean {

	private int stationId;
	private int vehicleId;
	private int hour;
	private int minute;

	public int getStationId() {
		return stationId;
	}

	public int getVehicleId() {
		return vehicleId;
	}

	public void setStationId(int stationId) {
		this.stationId = stationId;
	}

	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}

	public int getHour() {
		return hour;
	}

	public int getMinute() {
		return minute;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

}
