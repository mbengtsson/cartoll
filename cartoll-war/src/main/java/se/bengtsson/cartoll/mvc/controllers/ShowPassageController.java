package se.bengtsson.cartoll.mvc.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.services.PassageService;

@Controller
public class ShowPassageController {

	@Autowired
	private PassageService passageService;

	@RequestMapping("/showpassage/{passageId}")
	public ModelAndView cartolls(@PathVariable long passageId) {

		Passage passage = passageService.getPassage(passageId);

		ModelAndView mav = new ModelAndView("ShowPassage");
		mav.addObject("passage", passage);

		return mav;
	}
}
