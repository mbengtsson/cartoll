package se.bengtsson.cartoll.mvc.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.domain.statistic.Statistic;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;
import se.bengtsson.cartoll.services.OwnerService;
import se.bengtsson.cartoll.services.PassageService;
import se.bengtsson.cartoll.services.VehicleService;

@Controller
public class OwnerController {

	@Autowired
	private OwnerService ownerService;
	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private PassageService passageService;

	@RequestMapping("/owner/{ownerId}")
	public ModelAndView owner(@PathVariable long ownerId) {

		Owner owner = ownerService.getOwner(ownerId);
		Collection<Vehicle> vehicles = vehicleService.getVehiclesByOwner(ownerId);
		List<VehicleAndStatisticWrapper> vehicleStatistics = new ArrayList<>();

		for (Vehicle vehicle : vehicles) {
			vehicleStatistics.add(new VehicleAndStatisticWrapper(vehicle, passageService
					.getStatisticsByVehicle(vehicle)));
		}

		ModelAndView mav = new ModelAndView("Owner");
		mav.addObject("owner", owner);
		mav.addObject("vehicleStatistics", vehicleStatistics);

		return mav;
	}

	public class VehicleAndStatisticWrapper {

		private Vehicle vehicle;
		private Statistic statistic;

		public VehicleAndStatisticWrapper(Vehicle vehicle, Statistic statistic) {
			setVehicle(vehicle);
			setStatistic(statistic);

		}

		public Vehicle getVehicle() {
			return vehicle;
		}

		public Statistic getStatistic() {
			return statistic;
		}

		public void setVehicle(Vehicle vehicle) {
			this.vehicle = vehicle;
		}

		public void setStatistic(Statistic statistic) {
			this.statistic = statistic;
		}

	}
}
