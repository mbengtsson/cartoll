package se.bengtsson.cartoll.domain.vehicle.type;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

import se.bengtsson.cartoll.domain.owner.Owner;

public class TruckTest {

	private Owner owner;
	private Truck truck;

	@Before
	public void setup() throws ParseException {
		owner = new Owner("123456-7890", "John", "Doe", "Some street 24", "123 45", "Sometown");
		truck = new Truck("ABC-123", owner, 8.9);
	}

	@Test
	public void testTruck() {

		truck = new Truck("ABC-123", owner, 8.9);
		assertEquals("Reg-nr", "ABC-123", truck.getRegNr());
		assertEquals("Weight", 8.9, truck.getWeight(), 0.1);
		assertEquals("Type", "Lastbil", truck.getNiceType());
	}

	@Test
	public void testNiceName() throws Exception {
		truck = new Truck("ABC-123", owner, 8.9);
		assertEquals("Nice name", "Lastbil - ABC-123 (8.90 ton)", truck.getNiceName());

	}
}
