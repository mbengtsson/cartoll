package se.bengtsson.cartoll.domain.owner;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class OwnerTest {

	@Test
	public void testConstructor() {
		Owner owner = new Owner("123456-7890", "John", "Doe", "Some street 24", "123 45", "Sometown");
		assertEquals("SSN", "123456-7890", owner.getSocialSecurityNr());
		assertEquals("Full name", "John Doe", owner.getFullName());
		assertEquals("Street", "Some street 24", owner.getStreet());
		assertEquals("Zip-code", "123 45", owner.getZipCode());
		assertEquals("City", "Sometown", owner.getCity());

	}

}
