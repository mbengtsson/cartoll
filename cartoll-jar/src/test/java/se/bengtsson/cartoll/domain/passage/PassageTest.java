package se.bengtsson.cartoll.domain.passage;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.Test;

import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;
import se.bengtsson.cartoll.domain.vehicle.type.Car;

public class PassageTest {

	private Owner owner;
	private Vehicle car;
	private Station station;
	private DateTime time;

	@Before
	public void setup() throws ParseException {
		owner = new Owner("123456-7890", "John", "Doe", "Some street 24", "123 45", "Sometown");
		car = new Car("CAR-123", owner);
		station = new Station("Test-station 1");

		time = new LocalTime(6, 0).toDateTimeToday();

	}

	@Test
	public void testConstructor() {
		Passage passage = new Passage(station, car, time);
		assertEquals("Station", station, passage.getStation());
		assertEquals("Vehicle", car, passage.getVehicle());
		assertEquals("Time", time, passage.getTime());
	}

}
