package se.bengtsson.cartoll.domain.fee;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class FeeTest {

	@Test
	public void testFee() {
		Fee fee = new Fee("test", 20, 15, 10, 5, 0);
		assertEquals("test", fee.getType());
		assertEquals("High fee", 20, fee.getHighFee(), 0);
		assertEquals("Medium fee", 15, fee.getMediumFee(), 0);
		assertEquals("Low fee", 10, fee.getLowFee(), 0);
		assertEquals("Free fee", 5, fee.getFreeFee(), 0);
		assertEquals("Special fee", 0, fee.getSpecialFee(), 0);

	}

}
