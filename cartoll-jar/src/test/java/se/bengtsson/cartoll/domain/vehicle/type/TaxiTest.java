package se.bengtsson.cartoll.domain.vehicle.type;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

import se.bengtsson.cartoll.domain.owner.Owner;

public class TaxiTest {

	private Owner owner;
	private Taxi taxi1;
	private Taxi taxi2;

	@Before
	public void setup() throws ParseException {
		owner = new Owner("123456-7890", "John", "Doe", "Some street 24", "123 45", "Sometown");
		taxi1 = new Taxi("ABC-123", owner, true);
		taxi2 = new Taxi("ABC-234", owner, false);
	}

	@Test
	public void testTaxi() {
		taxi1 = new Taxi("ABC-123", owner, true);
		taxi2 = new Taxi("ABC-234", owner, false);
		assertEquals("Reg-nr", "ABC-123", taxi1.getRegNr());
		assertEquals("Owner", owner, taxi1.getOwner());
		assertEquals("env-cert", true, taxi1.isEnvCertificated());
		assertEquals("no env-cert", false, taxi2.isEnvCertificated());
		assertEquals("Type", "Taxi", taxi1.getNiceType());

	}

	@Test
	public void testNiceName() throws Exception {
		taxi1 = new Taxi("ABC-123", owner, true);
		taxi2 = new Taxi("ABC-234", owner, false);
		assertEquals("Nice name", "Taxi - ABC-123 (Miljöcertifierad)", taxi1.getNiceName());
		assertEquals("Nice name", "Taxi - ABC-234 (Ej miljöcertifierad)", taxi2.getNiceName());
	}

}
