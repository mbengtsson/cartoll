package se.bengtsson.cartoll.domain.station;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StationTest {

	@Test
	public void testConstructor() {
		Station station = new Station("Test-station");
		assertEquals("Name", "Test-station", station.getName());
	}

}
