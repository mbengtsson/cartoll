package se.bengtsson.cartoll.domain.vehicle.type;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;

import org.junit.Before;
import org.junit.Test;

import se.bengtsson.cartoll.domain.owner.Owner;

public class CarTest {

	private Owner owner;
	private Car car;

	@Before
	public void setup() throws ParseException {
		owner = new Owner("123456-7890", "John", "Doe", "Some street 24", "123 45", "Sometown");
		car = new Car("ABC-123", owner);
	}

	@Test
	public void testCar() throws Exception {
		car = new Car("ABC-123", owner);
		assertEquals("Reg-nr", "ABC-123", car.getRegNr());
		assertEquals("Owner", owner, car.getOwner());
		assertEquals("Type", "Bil", car.getNiceType());
	}

	@Test
	public void testNiceName() throws Exception {
		car = new Car("ABC-123", owner);
		assertEquals("Nice name", "Bil - ABC-123", car.getNiceName());
	}
}
