package se.bengtsson.cartoll.integration;

import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration({ "/applicationContext.xml", "/db_test.xml" })
public class JpaIntegrationTest extends AbstractIntegrationTest {

}
