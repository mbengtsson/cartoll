package se.bengtsson.cartoll.integration;

import static org.junit.Assert.assertEquals;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import se.bengtsson.cartoll.domain.fee.Fee;
import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;
import se.bengtsson.cartoll.domain.vehicle.type.Car;
import se.bengtsson.cartoll.domain.vehicle.type.Taxi;
import se.bengtsson.cartoll.domain.vehicle.type.Truck;
import se.bengtsson.cartoll.services.FeeService;
import se.bengtsson.cartoll.services.OwnerService;
import se.bengtsson.cartoll.services.PassageService;
import se.bengtsson.cartoll.services.StationService;
import se.bengtsson.cartoll.services.VehicleService;

@RunWith(SpringJUnit4ClassRunner.class)
public abstract class AbstractIntegrationTest {

	@Autowired
	private OwnerService ownerService;
	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private StationService stationService;
	@Autowired
	private PassageService passageService;
	@Autowired
	private FeeService feeService;

	private DateTime time1;
	private DateTime time2;
	private DateTime time3;
	private DateTime time4;
	private DateTime time5;

	@Before
	public void setup() {
		time1 = new LocalTime(7, 0).toDateTimeToday();
		time2 = new LocalTime(9, 0).toDateTimeToday();
		time3 = new LocalTime(11, 0).toDateTimeToday();
		time4 = new LocalTime(13, 0).toDateTimeToday();
		time5 = new LocalTime(15, 0).toDateTimeToday();

		passageService.removeAllPassages();
		stationService.removeAllStations();
		vehicleService.removeAllVehicles();
		ownerService.removeAllOwners();
		feeService.removeAllFees();

		feeService.createFee(new Fee("car", 18, 13, 8, 0, 0));
		feeService.createFee(new Fee("taxi", 20, 15, 10, 0, 5));
		feeService.createFee(new Fee("truck", 25, 20, 12, 5, 10));

	}

	@Test
	public void springTest() {

		// Create owner
		Owner owner =
				ownerService
						.createOwner(new Owner("123456-7890", "John", "Doe", "Some street 24", "123 45", "Sometown"));

		assertEquals("Number of owners", 1, ownerService.getAllOwners().size(), 0);

		// Create Stations
		Station station1 = stationService.createStation(new Station("test station 1"));
		Station station2 = stationService.createStation(new Station("test station 2"));

		assertEquals("Number of stations", 2, stationService.getAllStations().size(), 0);

		// Create Vehicles

		Vehicle car = vehicleService.createVehicle(new Car("CAR-123", owner));
		Vehicle taxiNonEco = vehicleService.createVehicle(new Taxi("CAB-123", owner, false));
		Vehicle taxiEco = vehicleService.createVehicle(new Taxi("CAB-234", owner, true));
		Vehicle truckLight = vehicleService.createVehicle(new Truck("TRC-123", owner, 5.0));
		Vehicle truckHeavy = vehicleService.createVehicle(new Truck("TRC-234", owner, 8.5));

		// Create Passages

		Passage passage1 = passageService.createPassage(new Passage(station1, car, time1));
		Passage passage2 = passageService.createPassage(new Passage(station2, taxiNonEco, time2));
		Passage passage3 = passageService.createPassage(new Passage(station1, taxiEco, time3));
		Passage passage4 = passageService.createPassage(new Passage(station2, truckLight, time4));
		Passage passage5 = passageService.createPassage(new Passage(station1, truckHeavy, time5));

		// Test fees

		assertEquals("Passage 1 fee", 18, passage1.getFee(), 0);
		assertEquals("Passage 2 fee", 15, passage2.getFee(), 0);
		assertEquals("Passage 3 fee", 10, passage3.getFee(), 0);
		assertEquals("Passage 4 fee", 12, passage4.getFee(), 0);
		assertEquals("Passage 5 fee", 40, passage5.getFee(), 0);

	}

}
