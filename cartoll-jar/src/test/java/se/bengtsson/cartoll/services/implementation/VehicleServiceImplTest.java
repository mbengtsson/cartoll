package se.bengtsson.cartoll.services.implementation;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import se.bengtsson.cartoll.database.VehicleRepo;
import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;
import se.bengtsson.cartoll.domain.vehicle.type.Car;
import se.bengtsson.cartoll.services.VehicleService;

public class VehicleServiceImplTest {

	private Owner owner;
	private Vehicle vehicle;
	private Collection<Vehicle> vehicles;
	private VehicleService vehicleService;

	@Before
	public void setup() {
		owner = new Owner("123456-7890", "John", "Doe", "Some street 24", "123 45", "Sometown");
		vehicle = new Car("ABC-123", owner);
		vehicles = new ArrayList<>();
		vehicles.add(vehicle);
		vehicleService = new VehicleServiceImpl();

	}

	@Test
	public void testCreateVehicle() {

		// Setup mock repo
		VehicleRepo repo = createMock(VehicleRepo.class);
		expect(repo.createVehicle(vehicle)).andReturn(1l);
		expect(repo.getVehicle(1)).andReturn(vehicle);
		replay(repo);
		vehicleService.setVehicleRepo(repo);

		// Perform test
		assertEquals("Get vehicle in return", vehicle, vehicleService.createVehicle(vehicle));

		// Verify
		verify(repo);
	}

	@Test
	public void testUpdateVehicle() {
		// Setup mock repo
		VehicleRepo repo = createMock(VehicleRepo.class);
		repo.updateVehicle(vehicle);
		replay(repo);
		vehicleService.setVehicleRepo(repo);

		// Perform test
		vehicleService.updateVehicle(vehicle);

		// Verify
		verify(repo);

	}

	@Test
	public void testGetVehicle() {
		// Setup mock repo
		VehicleRepo repo = createMock(VehicleRepo.class);
		expect(repo.getVehicle(1)).andReturn(vehicle);
		replay(repo);
		vehicleService.setVehicleRepo(repo);

		// Perform test
		assertEquals("Get vehicle in return", vehicle, vehicleService.getVehicle(1));

		// Verify
		verify(repo);
	}

	@Test
	public void testGetAllVehicles() {
		// Setup mock repo
		VehicleRepo repo = createMock(VehicleRepo.class);
		expect(repo.getAllVehicles()).andReturn(vehicles);
		replay(repo);
		vehicleService.setVehicleRepo(repo);

		// Perform test
		assertEquals("Get all vehicle in return", vehicles, vehicleService.getAllVehicles());

		//
		verify(repo);

	}

	@Test
	public void testRemoveVehicle() {
		// Setup mock repo
		VehicleRepo repo = createMock(VehicleRepo.class);
		expect(repo.removeVehicle(1)).andReturn(vehicle);
		replay(repo);
		vehicleService.setVehicleRepo(repo);

		// Perform test
		assertEquals("Get vehicle in return", vehicle, vehicleService.removeVehicle(1));

		// verify
		verify(repo);
	}

	@Test
	public void testRemoveAllVehicles() {

		// Setup mock repo
		VehicleRepo repo = createMock(VehicleRepo.class);
		repo.removeAllVehicles();
		replay(repo);
		vehicleService.setVehicleRepo(repo);

		// Perform test
		vehicleService.removeAllVehicles();

		// Verify
		verify(repo);
	}

	@Test
	public void testGetVehiclesByOwner() {
		// Setup mock repo
		VehicleRepo repo = createMock(VehicleRepo.class);
		expect(repo.getVehiclesByOwner(1)).andReturn(vehicles);
		replay(repo);
		vehicleService.setVehicleRepo(repo);

		// Perform test
		assertEquals("Get vehicles by owner", vehicles, vehicleService.getVehiclesByOwner(1));

		// Verify
		verify(repo);
	}

}
