package se.bengtsson.cartoll.services.implementation;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import se.bengtsson.cartoll.database.OwnerRepo;
import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.services.OwnerService;

public class OwnerServiceImplTest {

	private Owner owner;
	private Collection<Owner> owners;
	private OwnerService ownerService;

	@Before
	public void setup() {
		owner = new Owner("123456-7890", "John", "Doe", "Some street 24", "123 45", "Sometown");
		owners = new ArrayList<>();
		owners.add(owner);
		ownerService = new OwnerServiceImpl();
	}

	@Test
	public void testCreateOwner() {

		// Setup mock repo
		OwnerRepo repo = createMock(OwnerRepo.class);
		expect(repo.createOwner(owner)).andReturn(1l);
		expect(repo.getOwner(1)).andReturn(owner);
		replay(repo);
		ownerService.setOwnerRepo(repo);

		// Perform test
		assertEquals("Get owner in return", owner, ownerService.createOwner(owner));

		// Verify
		verify(repo);
	}

	@Test
	public void testUpdateOwner() {

		// Setup mock repo
		OwnerRepo repo = createMock(OwnerRepo.class);
		repo.updateOwner(owner);
		replay(repo);
		ownerService.setOwnerRepo(repo);

		// Perform test
		ownerService.updateOwner(owner);

		// Verify
		verify(repo);
	}

	@Test
	public void testGetOwner() {
		// Setup mock repo
		OwnerRepo repo = createMock(OwnerRepo.class);
		expect(repo.getOwner(1)).andReturn(owner);
		replay(repo);
		ownerService.setOwnerRepo(repo);

		// Perform test
		assertEquals("Get owner in return", owner, ownerService.getOwner(1));

		// Verify
		verify(repo);

	}

	@Test
	public void testGetAllOwners() {
		// Setup mock repo
		OwnerRepo repo = createMock(OwnerRepo.class);
		expect(repo.getAllOwners()).andReturn(owners);
		replay(repo);
		ownerService.setOwnerRepo(repo);

		// Perform test
		assertEquals("Get all owners in return", owners, ownerService.getAllOwners());

		// Verify
		verify(repo);
	}

	@Test
	public void testRemoveOwner() {
		// Setup mock repo
		OwnerRepo repo = createMock(OwnerRepo.class);
		expect(repo.removeOwner(1)).andReturn(owner);
		replay(repo);
		ownerService.setOwnerRepo(repo);

		// Perform test
		assertEquals("Get owner in return", owner, ownerService.removeOwner(1));

		// Verify
		verify(repo);

	}

	@Test
	public void testRemoveAllOwners() {

		// Setup mock repo
		OwnerRepo repo = createMock(OwnerRepo.class);
		repo.removeAllOwners();
		replay(repo);
		ownerService.setOwnerRepo(repo);

		// Perform test
		ownerService.removeAllOwners();

		// Verify
		verify(repo);
	}

}
