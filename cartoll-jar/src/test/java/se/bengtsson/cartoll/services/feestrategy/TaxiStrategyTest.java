package se.bengtsson.cartoll.services.feestrategy;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.Test;

import se.bengtsson.cartoll.domain.fee.Fee;
import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;
import se.bengtsson.cartoll.domain.vehicle.type.Taxi;

public class TaxiStrategyTest {

	private TaxiStrategy strat;

	private DateTime freeTime;
	private DateTime lowTime;
	private DateTime midTime;
	private DateTime highTime;

	private Owner owner;
	private Vehicle noEnvTaxi;
	private Vehicle envTaxi;

	private Station station;

	private Passage noEnvFreePass;
	private Passage noEnvLowPass;
	private Passage noEnvMidPass;
	private Passage noEnvHighPass;

	private Passage envFreePass;
	private Passage envLowPass;
	private Passage envMidPass;
	private Passage envHighPass;

	private Fee fee;

	@Before
	public void setup() throws ParseException {

		freeTime = new LocalTime(0, 0).toDateTimeToday();
		lowTime = new LocalTime(11, 11).toDateTimeToday();
		midTime = new LocalTime(15, 15).toDateTimeToday();
		highTime = new LocalTime(7, 7).toDateTimeToday();

		owner = new Owner("123456-7890", "John", "Doe", "Somewhere", "123 45", "Sometown");
		noEnvTaxi = new Taxi("CAB-123", owner, false);
		envTaxi = new Taxi("CAB-234", owner, true);

		station = new Station("test-station");

		noEnvFreePass = new Passage(station, noEnvTaxi, freeTime);
		noEnvLowPass = new Passage(station, noEnvTaxi, lowTime);
		noEnvMidPass = new Passage(station, noEnvTaxi, midTime);
		noEnvHighPass = new Passage(station, noEnvTaxi, highTime);

		envFreePass = new Passage(station, envTaxi, freeTime);
		envLowPass = new Passage(station, envTaxi, lowTime);
		envMidPass = new Passage(station, envTaxi, midTime);
		envHighPass = new Passage(station, envTaxi, highTime);

		fee = new Fee("taxi", 20, 15, 10, 0, 5);

	}

	@Test
	public void testGetHighRangeFee() {
		strat = new TaxiStrategy(envHighPass, fee);
		assertEquals("High fee", 20, strat.getHighRangeFee(), 0);
		strat = new TaxiStrategy(noEnvHighPass, fee);
		assertEquals("High fee + env fee", 25, strat.getHighRangeFee(), 0);
	}

	@Test
	public void testGetMediumRangeFee() {
		strat = new TaxiStrategy(envMidPass, fee);
		assertEquals("Mid fee", 15, strat.getMediumRangeFee(), 0);
		strat = new TaxiStrategy(noEnvMidPass, fee);
		assertEquals("Mid fee + env fee", 20, strat.getMediumRangeFee(), 0);
	}

	@Test
	public void testGetLowRangeFee() {
		strat = new TaxiStrategy(envLowPass, fee);
		assertEquals("Low fee", 10, strat.getLowRangeFee(), 0);
		strat = new TaxiStrategy(noEnvLowPass, fee);
		assertEquals("Low fee + env fee", 15, strat.getLowRangeFee(), 0);
	}

	@Test
	public void testGetFreeRangeFee() {
		strat = new TaxiStrategy(envFreePass, fee);
		assertEquals("Free fee", 0, strat.getFreeRangeFee(), 0);
		strat = new TaxiStrategy(noEnvFreePass, fee);
		assertEquals("Free fee + env fee", 0, strat.getFreeRangeFee(), 0);
	}

	@Test
	public void testCalculateFee() {
		strat = new TaxiStrategy(noEnvFreePass, fee);
		assertEquals("Free fee + env fee", 0, strat.calculateFee(), 0);
		strat = new TaxiStrategy(envFreePass, fee);
		assertEquals("Free fee", 0, strat.getFreeRangeFee(), 0);

		strat = new TaxiStrategy(noEnvLowPass, fee);
		assertEquals("Low fee + env fee", 15, strat.calculateFee(), 0);
		strat = new TaxiStrategy(envLowPass, fee);
		assertEquals("Low fee", 10, strat.getLowRangeFee(), 0);

		strat = new TaxiStrategy(noEnvMidPass, fee);
		assertEquals("Mid fee + env fee", 20, strat.calculateFee(), 0);
		strat = new TaxiStrategy(envMidPass, fee);
		assertEquals("Mid fee", 15, strat.getMediumRangeFee(), 0);

		strat = new TaxiStrategy(noEnvHighPass, fee);
		assertEquals("High fee + env fee", 25, strat.calculateFee(), 0);
		strat = new TaxiStrategy(envHighPass, fee);
		assertEquals("High fee", 20, strat.getHighRangeFee(), 0);

	}

}
