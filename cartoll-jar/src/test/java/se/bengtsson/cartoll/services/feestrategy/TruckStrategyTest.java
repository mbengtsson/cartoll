package se.bengtsson.cartoll.services.feestrategy;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.Test;

import se.bengtsson.cartoll.domain.fee.Fee;
import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;
import se.bengtsson.cartoll.domain.vehicle.type.Truck;

public class TruckStrategyTest {

	private TruckStrategy strat;

	private DateTime freeTime;
	private DateTime lowTime;
	private DateTime midTime;
	private DateTime highTime;

	private Owner owner;
	private Vehicle heavyTruck;
	private Vehicle lightTruck;

	private Station station;

	private Passage heavyFreePass;
	private Passage heavyLowPass;
	private Passage heavyMidPass;
	private Passage heavyHighPass;

	private Passage lightFreePass;
	private Passage lightLowPass;
	private Passage lightMidPass;
	private Passage lightHighPass;

	Fee fee;

	@Before
	public void setup() throws ParseException {

		freeTime = new LocalTime(0, 0).toDateTimeToday();
		lowTime = new LocalTime(11, 11).toDateTimeToday();
		midTime = new LocalTime(15, 15).toDateTimeToday();
		highTime = new LocalTime(7, 7).toDateTimeToday();

		owner = new Owner("123456-7890", "John", "Doe", "Somewhere", "123 45", "Sometown");
		heavyTruck = new Truck("CAB-123", owner, 9.5);
		lightTruck = new Truck("CAB-234", owner, 3.4);

		station = new Station("test-station");

		heavyFreePass = new Passage(station, heavyTruck, freeTime);
		heavyLowPass = new Passage(station, heavyTruck, lowTime);
		heavyMidPass = new Passage(station, heavyTruck, midTime);
		heavyHighPass = new Passage(station, heavyTruck, highTime);

		lightFreePass = new Passage(station, lightTruck, freeTime);
		lightLowPass = new Passage(station, lightTruck, lowTime);
		lightMidPass = new Passage(station, lightTruck, midTime);
		lightHighPass = new Passage(station, lightTruck, highTime);

		fee = new Fee("truck", 25, 20, 12, 5, 10);

	}

	@Test
	public void testGetHighRangeFee() {
		strat = new TruckStrategy(lightHighPass, fee);
		assertEquals("High fee", 25, strat.getHighRangeFee(), 0);
		strat = new TruckStrategy(heavyHighPass, fee);
		assertEquals("High fee + weight fee", 55, strat.getHighRangeFee(), 0);
	}

	@Test
	public void testGetMediumRangeFee() {
		strat = new TruckStrategy(lightMidPass, fee);
		assertEquals("Mid fee", 20, strat.getMediumRangeFee(), 0);
		strat = new TruckStrategy(heavyMidPass, fee);
		assertEquals("Mid fee + weight fee", 50, strat.getMediumRangeFee(), 0);
	}

	@Test
	public void testGetLowRangeFee() {
		strat = new TruckStrategy(lightLowPass, fee);
		assertEquals("Low fee", 12, strat.getLowRangeFee(), 0);
		strat = new TruckStrategy(heavyLowPass, fee);
		assertEquals("Low fee + weight fee", 42, strat.getLowRangeFee(), 0);
	}

	@Test
	public void testGetFreeRangeFee() {
		strat = new TruckStrategy(lightFreePass, fee);
		assertEquals("Free fee", 5, strat.getFreeRangeFee(), 0);
		strat = new TruckStrategy(heavyFreePass, fee);
		assertEquals("Free fee + weight fee", 35, strat.getFreeRangeFee(), 0);
	}

	@Test
	public void testCalculateFee() {
		strat = new TruckStrategy(heavyFreePass, fee);
		assertEquals("Free fee + weight fee", 35, strat.calculateFee(), 0);
		strat = new TruckStrategy(lightFreePass, fee);
		assertEquals("Free fee", 5, strat.getFreeRangeFee(), 0);

		strat = new TruckStrategy(heavyLowPass, fee);
		assertEquals("Low fee + weight fee", 42, strat.calculateFee(), 0);
		strat = new TruckStrategy(lightLowPass, fee);
		assertEquals("Low fee", 12, strat.getLowRangeFee(), 0);

		strat = new TruckStrategy(heavyMidPass, fee);
		assertEquals("Mid fee + weight fee", 50, strat.calculateFee(), 0);
		strat = new TruckStrategy(lightMidPass, fee);
		assertEquals("Mid fee", 20, strat.getMediumRangeFee(), 0);

		strat = new TruckStrategy(heavyHighPass, fee);
		assertEquals("High fee + weight fee", 55, strat.calculateFee(), 0);
		strat = new TruckStrategy(lightHighPass, fee);
		assertEquals("High fee", 25, strat.getHighRangeFee(), 0);

	}

}
