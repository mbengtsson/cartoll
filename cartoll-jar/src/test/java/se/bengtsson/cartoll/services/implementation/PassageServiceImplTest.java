package se.bengtsson.cartoll.services.implementation;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.Test;

import se.bengtsson.cartoll.database.PassageRepo;
import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.statistic.Statistic;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;
import se.bengtsson.cartoll.domain.vehicle.type.Car;
import se.bengtsson.cartoll.services.FeeService;
import se.bengtsson.cartoll.services.PassageService;

public class PassageServiceImplTest {

	private Owner owner;
	private Vehicle vehicle;
	private DateTime time;
	private Station station;
	private Passage passage;
	private Collection<Passage> passages;
	private Statistic statistic;
	private PassageService passageService;

	@Before
	public void setup() {
		owner = new Owner("123456-7890", "John", "Doe", "Some street 24", "123 45", "Sometown");
		vehicle = new Car("ABC-123", owner);
		station = new Station("test-station");
		time = new LocalTime(13, 32).toDateTimeToday();
		passage = new Passage(station, vehicle, time);
		passages = new ArrayList<>();
		passages.add(passage);
		passageService = new PassageServiceImpl();
		statistic = new Statistic(4l, 5l);

	}

	@Test
	public void testCreatePassage() {

		// Setup mock service
		FeeService service = createMock(FeeService.class);
		expect(service.getFeeForPassage(passage)).andReturn(8);
		replay(service);
		passageService.setFeeService(service);

		// Setup mock repo
		PassageRepo repo = createMock(PassageRepo.class);
		expect(repo.createPassage(passage)).andReturn(1l);
		expect(repo.getPassage(1)).andReturn(passage);
		replay(repo);
		passageService.setPassageRepo(repo);

		// Perform test
		assertEquals("Get passage in return", passage, passageService.createPassage(passage));
		assertEquals("Added fee", 8, passage.getFee(), 0);

		// Verify
		verify(service);
		verify(repo);

	}

	@Test
	public void testUpdatePassage() {

		// Setup mock repo
		PassageRepo repo = createMock(PassageRepo.class);
		repo.updatePassage(passage);
		replay(repo);
		passageService.setPassageRepo(repo);

		// Perform test
		passageService.updatePassage(passage);

		// Verify
		verify(repo);
	}

	@Test
	public void testGetPassage() {

		// Setup mock repo
		PassageRepo repo = createMock(PassageRepo.class);
		expect(repo.getPassage(1)).andReturn(passage);
		replay(repo);
		passageService.setPassageRepo(repo);

		// Perform test
		assertEquals("Get passage in return", passage, passageService.getPassage(1));

		// Verify
		verify(repo);
	}

	@Test
	public void testGetAllPassages() {

		// Setup mock repo
		PassageRepo repo = createMock(PassageRepo.class);
		expect(repo.getAllPassages()).andReturn(passages);
		replay(repo);
		passageService.setPassageRepo(repo);

		// Perform test
		assertEquals("Get all passage in return", passages, passageService.getAllPassages());

		// Verify
		verify(repo);
	}

	@Test
	public void testRemovePassage() {

		// Setup mock repo
		PassageRepo repo = createMock(PassageRepo.class);
		expect(repo.removePassage(1)).andReturn(passage);
		replay(repo);
		passageService.setPassageRepo(repo);

		// Perform test
		assertEquals("Get passage in return", passage, passageService.removePassage(1));

		// Verify
		verify(repo);
	}

	@Test
	public void testRemoveAllPassages() {

		// Setup mock repo
		PassageRepo repo = createMock(PassageRepo.class);
		repo.removeAllPassages();
		replay(repo);
		passageService.setPassageRepo(repo);

		// Perform test
		passageService.removeAllPassages();

		// Verify
		verify(repo);
	}

	@Test
	public void testGetPassagesByVehicle() {

		// Setup mock repo
		PassageRepo repo = createMock(PassageRepo.class);
		expect(repo.getPassagesByVehicle(1)).andReturn(passages);
		replay(repo);
		passageService.setPassageRepo(repo);

		// Perform test
		assertEquals("Get passages by vehicle", passages, passageService.getPassagesByVehicle(1));

		// Verify
		verify(repo);

	}

	@Test
	public void testGetPassagesByStation() {

		// Setup mock repo
		PassageRepo repo = createMock(PassageRepo.class);
		expect(repo.getPassagesByStation(1)).andReturn(passages);
		replay(repo);
		passageService.setPassageRepo(repo);

		// Perform test
		assertEquals("Get passages by station", passages, passageService.getPassagesByStation(1));

		// Verify
		verify(repo);

	}

	@Test
	public void testTotalStatistics() throws Exception {
		// Setup mock repo
		PassageRepo repo = createMock(PassageRepo.class);
		expect(repo.getTotalStatistics()).andReturn(statistic);
		replay(repo);
		passageService.setPassageRepo(repo);

		// Perform test
		assertEquals("Get total statistics", statistic, passageService.getTotalStatistics());

		// Verify
		verify(repo);
	}

	@Test
	public void testGetStatisticsByStation() throws Exception {
		// Setup mock repo
		PassageRepo repo = createMock(PassageRepo.class);
		expect(repo.getStatisticsByStation(station)).andReturn(statistic);
		replay(repo);
		passageService.setPassageRepo(repo);

		// Perform test
		assertEquals("Get statistics by station", statistic, passageService.getStatisticsByStation(station));

		// Verify
		verify(repo);
	}

	@Test
	public void testGetStatisticsByVehicle() throws Exception {
		// Setup mock repo
		PassageRepo repo = createMock(PassageRepo.class);
		expect(repo.getStatisticsByVehicle(vehicle)).andReturn(statistic);
		replay(repo);
		passageService.setPassageRepo(repo);

		// Perform test
		assertEquals("Get statistics by vehicle", statistic, passageService.getStatisticsByVehicle(vehicle));

		// Verify
		verify(repo);
	}

}
