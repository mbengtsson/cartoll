package se.bengtsson.cartoll.services.implementation;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.Test;

import se.bengtsson.cartoll.database.FeeRepo;
import se.bengtsson.cartoll.domain.fee.Fee;
import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;
import se.bengtsson.cartoll.domain.vehicle.type.Car;
import se.bengtsson.cartoll.services.FeeService;

public class FeeServiceImplTest {

	private Fee fee;
	private Owner owner;
	private Station station;
	private Vehicle vehicle;
	private DateTime time;
	private Passage passage;
	private Collection<Fee> fees;
	private FeeService feeService;

	@Before
	public void setup() {
		fee = new Fee("test", 20, 15, 10, 0, 5);
		owner = new Owner("123456-7890", "John", "Doe", "Some street 24", "123 45", "Sometown");
		station = new Station("Test-station");
		vehicle = new Car("ABC-123", owner);
		time = new LocalTime(13, 32).toDateTimeToday();
		passage = new Passage(station, vehicle, time);
		fees = new ArrayList<>();
		feeService = new FeeServiceImpl();

	}

	@Test
	public void testCreateFee() {
		// Setup mock repo
		FeeRepo repo = createMock(FeeRepo.class);
		expect(repo.createFee(fee)).andReturn("test");
		expect(repo.getFee("test")).andReturn(fee);
		replay(repo);
		feeService.setFeeRepo(repo);

		// Perform test
		assertEquals("Get fee in return", fee, feeService.createFee(fee));

		// Verify
		verify(repo);
	}

	@Test
	public void testUpdateFee() {
		// Setup mock repo
		FeeRepo repo = createMock(FeeRepo.class);
		repo.updateFee(fee);
		replay(repo);
		feeService.setFeeRepo(repo);

		// Perform test
		feeService.updateFee(fee);

		// Verify
		verify(repo);
	}

	@Test
	public void testGetFee() {
		// Setup mock repo
		FeeRepo repo = createMock(FeeRepo.class);
		expect(repo.getFee("test")).andReturn(fee);
		replay(repo);
		feeService.setFeeRepo(repo);

		// Perform test
		assertEquals("Get fee in return", fee, feeService.getFee("test"));

		// Verify
		verify(repo);
	}

	@Test
	public void testGetAllFees() {
		// Setup mock repo
		FeeRepo repo = createMock(FeeRepo.class);
		expect(repo.getAllFees()).andReturn(fees);
		replay(repo);
		feeService.setFeeRepo(repo);

		// Perform test
		assertEquals("Get all fees in return", fees, feeService.getAllFees());

		// Verify
		verify(repo);
	}

	@Test
	public void testRemoveFee() {
		// Setup mock repo
		FeeRepo repo = createMock(FeeRepo.class);
		expect(repo.removeFee("test")).andReturn(fee);
		replay(repo);
		feeService.setFeeRepo(repo);

		// Perform test
		assertEquals("Get fee in return", fee, feeService.removeFee("test"));

		// Verify
		verify(repo);

	}

	@Test
	public void testRemoveAllFees() {

		// Setup mock repo
		FeeRepo repo = createMock(FeeRepo.class);
		repo.removeAllFees();
		replay(repo);
		feeService.setFeeRepo(repo);

		// Perform test
		feeService.removeAllFees();

		// Verify
		verify(repo);
	}

	@Test
	public void testGetFeeForPassage() {
		// Setup mock repo
		FeeRepo repo = createMock(FeeRepo.class);
		expect(repo.getFee("car")).andReturn(fee);
		replay(repo);
		feeService.setFeeRepo(repo);

		// Perform test
		assertEquals("Fee for passage", 10, feeService.getFeeForPassage(passage), 0);

		// Verify
		verify(repo);
	}

}
