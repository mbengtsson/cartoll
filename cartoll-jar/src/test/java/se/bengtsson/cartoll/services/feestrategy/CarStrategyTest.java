package se.bengtsson.cartoll.services.feestrategy;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;
import org.junit.Before;
import org.junit.Test;

import se.bengtsson.cartoll.domain.fee.Fee;
import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;
import se.bengtsson.cartoll.domain.vehicle.type.Car;

public class CarStrategyTest {

	private CarStrategy strat;

	private DateTime freeTime;
	private DateTime lowTime;
	private DateTime midTime;
	private DateTime highTime;

	private Owner owner;
	private Vehicle car;

	private Station station;

	private Passage freePass;
	private Passage lowPass;
	private Passage midPass;
	private Passage highPass;

	private Fee fee;

	@Before
	public void setup() throws ParseException {

		freeTime = new LocalTime(0, 0).toDateTimeToday();
		lowTime = new LocalTime(11, 11).toDateTimeToday();
		midTime = new LocalTime(15, 15).toDateTimeToday();
		highTime = new LocalTime(7, 7).toDateTimeToday();

		owner = new Owner("123456-7890", "John", "Doe", "Somewhere", "123 45", "Sometown");
		car = new Car("ABC-123", owner);

		station = new Station("test-station");

		freePass = new Passage(station, car, freeTime);
		lowPass = new Passage(station, car, lowTime);
		midPass = new Passage(station, car, midTime);
		highPass = new Passage(station, car, highTime);

		fee = new Fee("car", 18, 13, 8, 0, 0);

	}

	@Test
	public void testGetHighRangeFee() {
		strat = new CarStrategy(highPass, fee);
		assertEquals("High fee", 18, strat.getHighRangeFee(), 0);
	}

	@Test
	public void testGetMediumRangeFee() {
		strat = new CarStrategy(midPass, fee);
		assertEquals("Mid fee", 13, strat.getMediumRangeFee(), 0);
	}

	@Test
	public void testGetLowRangeFee() {
		strat = new CarStrategy(lowPass, fee);
		assertEquals("Low fee", 8, strat.getLowRangeFee(), 0);
	}

	@Test
	public void testGetFreeRangeFee() {
		strat = new CarStrategy(freePass, fee);
		assertEquals("Free fee", 0, strat.getFreeRangeFee(), 0);
	}

	@Test
	public void testCalculateFee() {
		strat = new CarStrategy(freePass, fee);
		assertEquals("Free fee", 0, strat.calculateFee(), 0);

		strat = new CarStrategy(lowPass, fee);
		assertEquals("Low fee", 8, strat.calculateFee(), 0);

		strat = new CarStrategy(midPass, fee);
		assertEquals("Mid fee", 13, strat.calculateFee(), 0);

		strat = new CarStrategy(highPass, fee);
		assertEquals("High fee", 18, strat.calculateFee(), 0);
	}
}
