package se.bengtsson.cartoll.services.implementation;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import se.bengtsson.cartoll.database.StationRepo;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.services.StationService;

public class StationServiceImplTest {

	private Station station;
	private Collection<Station> stations;
	private StationService stationService;

	@Before
	public void setup() {
		station = new Station("test-station");
		stations = new ArrayList<>();
		stations.add(station);
		stationService = new StationServiceImpl();
	}

	@Test
	public void testCreateStation() {

		// Setup mock repo
		StationRepo repo = createMock(StationRepo.class);
		expect(repo.createStation(station)).andReturn(1l);
		expect(repo.getStation(1)).andReturn(station);
		replay(repo);
		stationService.setStationRepo(repo);

		// Perform test
		assertEquals("Get station in return", station, stationService.createStation(station));

		// Verify
		verify(repo);
	}

	@Test
	public void testUpdateStation() {

		// Setup mock repo
		StationRepo repo = createMock(StationRepo.class);
		repo.updateStation(station);
		replay(repo);
		stationService.setStationRepo(repo);

		// Perform test
		stationService.updateStation(station);

		// Verify
		verify(repo);
	}

	@Test
	public void testGetStation() {

		// Setup mock repo
		StationRepo repo = createMock(StationRepo.class);
		expect(repo.getStation(1)).andReturn(station);
		replay(repo);
		stationService.setStationRepo(repo);

		// Perform test
		assertEquals("Get station in return", station, stationService.getStation(1));

		// Verify
		verify(repo);

	}

	@Test
	public void testGetAllStations() {

		// Setup mock repo
		StationRepo repo = createMock(StationRepo.class);
		expect(repo.getAllStations()).andReturn(stations);
		replay(repo);
		stationService.setStationRepo(repo);

		// Perform test
		assertEquals("Get all station in return", stations, stationService.getAllStations());

		// Verify
		verify(repo);
	}

	@Test
	public void testRemoveStation() {

		// Setup mock repo
		StationRepo repo = createMock(StationRepo.class);
		expect(repo.removeStation(1)).andReturn(station);
		replay(repo);
		stationService.setStationRepo(repo);

		// Perform test
		assertEquals("Get station in return", station, stationService.removeStation(1));

		// Verify
		verify(repo);
	}

	@Test
	public void testRemoveAllStations() {

		// Setup mock repo
		StationRepo repo = createMock(StationRepo.class);
		repo.removeAllStations();
		replay(repo);
		stationService.setStationRepo(repo);

		// Perform test
		stationService.removeAllStations();

		// Verify
		verify(repo);
	}
}
