package se.bengtsson.cartoll.database.jpa;

import java.util.Collection;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import se.bengtsson.cartoll.database.PassageRepo;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.statistic.Statistic;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;

public class JpaPassageRepo extends AbstractJpaRepository implements PassageRepo {

	Logger log = Logger.getLogger(JpaPassageRepo.class);

	@Override
	@Transactional
	public long createPassage(Passage passage) {
		em.persist(passage);
		log.debug("JPA: Created passage in db: " + passage);
		return passage.getId();
	}

	@Override
	@Transactional
	public void updatePassage(Passage passage) {
		em.merge(passage);
		log.debug("JPA: Updated passage in db: " + passage);
	}

	@Override
	public Passage getPassage(long id) {
		Passage passage = em.find(Passage.class, id);
		log.debug("JPA: Got passage from db: " + passage);
		return passage;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Passage> getAllPassages() {
		Query query = em.createNamedQuery("getAllPassages");
		log.debug("JPA: Got all passages from db");
		return query.getResultList();
	}

	@Override
	@Transactional
	public Passage removePassage(long id) {
		Passage passage = getPassage(id);
		em.remove(passage);
		log.debug("JPA: Removed passage from db: " + passage);
		return passage;
	}

	@Override
	@Transactional
	public void removeAllPassages() {
		Query query = em.createNamedQuery("removeAllPassages");
		query.executeUpdate();
		log.debug("JPA: Removed all passages from db");

	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Passage> getPassagesByVehicle(long id) {
		Query query = em.createNamedQuery("getPassagesByVehicle");
		query.setParameter("id", id);
		log.debug("JPA: Got passages by vehicle with id " + id + " from db");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Passage> getPassagesByStation(long id) {
		Query query = em.createNamedQuery("getPassagesByStation");
		query.setParameter("id", id);
		log.debug("JPA: Got passages by station with id " + id + " from db");
		return query.getResultList();
	}

	@Override
	public Statistic getTotalStatistics() {
		Query query =
				em.createQuery("SELECT NEW se.bengtsson.cartoll.domain.statistic.Statistic(sum(p.fee), count(p) ) FROM Passage p");
		log.debug("JPA: Got total statistics from db");
		return (Statistic) query.getSingleResult();

	}

	@Override
	public Statistic getStatisticsByStation(Station station) {
		Query query =
				em.createQuery("SELECT NEW se.bengtsson.cartoll.domain.statistic.Statistic(sum(p.fee), count(p) ) FROM Passage p WHERE station_id = :id");
		query.setParameter("id", station.getId());
		log.debug("JPA: Got statistics by station with id: " + station.getId() + " from db");
		return (Statistic) query.getSingleResult();

	}

	@Override
	public Statistic getStatisticsByVehicle(Vehicle vehicle) {
		Query query =
				em.createQuery("SELECT NEW se.bengtsson.cartoll.domain.statistic.Statistic(sum(p.fee), count(p) ) FROM Passage p WHERE vehicle_id = :id");
		query.setParameter("id", vehicle.getId());
		log.debug("JPA: Got statistics by vehicle with id: " + vehicle.getId() + " from db");
		return (Statistic) query.getSingleResult();

	}

}
