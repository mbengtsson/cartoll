package se.bengtsson.cartoll.database;

import java.util.Collection;

import se.bengtsson.cartoll.domain.fee.Fee;

public interface FeeRepo {

	public String createFee(Fee fee);

	public void updateFee(Fee fee);

	public Fee getFee(String type);

	public Collection<Fee> getAllFees();

	public Fee removeFee(String type);

	public void removeAllFees();

}
