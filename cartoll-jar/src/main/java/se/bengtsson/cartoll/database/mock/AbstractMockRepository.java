package se.bengtsson.cartoll.database.mock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

public abstract class AbstractMockRepository implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	private MockDataFiller mockDataFiller;

	private static long nextId = 1;

	protected long getNextId() {
		return nextId++;
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		mockDataFiller.initilizeMockDataIfNecessary();

	}

}
