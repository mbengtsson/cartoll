package se.bengtsson.cartoll.database.jpa;

import java.util.Collection;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import se.bengtsson.cartoll.database.FeeRepo;
import se.bengtsson.cartoll.domain.fee.Fee;

public class JpaFeeRepo extends AbstractJpaRepository implements FeeRepo {

	Logger log = Logger.getLogger(JpaFeeRepo.class);

	@Override
	@Transactional
	public String createFee(Fee fee) {
		em.persist(fee);
		log.debug("JPA: Created fee in db: " + fee);
		return fee.getType();
	}

	@Override
	@Transactional
	public void updateFee(Fee fee) {
		em.merge(fee);
		log.debug("JPA: Updated fee in db: " + fee);

	}

	@Override
	public Fee getFee(String type) {
		Fee fee = em.find(Fee.class, type);
		log.debug("JPA: Got fee from db: " + fee);
		return fee;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Fee> getAllFees() {
		Query query = em.createNamedQuery("getAllFees");
		log.debug("JPA: Got all fees from db");
		return query.getResultList();
	}

	@Override
	@Transactional
	public Fee removeFee(String type) {
		Fee fee = getFee(type);
		em.remove(fee);
		log.debug("JPA: Removed fee from db: " + fee);
		return fee;
	}

	@Override
	@Transactional
	public void removeAllFees() {
		Query query = em.createNamedQuery("removeAllFees");
		query.executeUpdate();
		log.debug("JPA: Removed all fees from db");
	}

}
