package se.bengtsson.cartoll.database.mock;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

import se.bengtsson.cartoll.database.PassageRepo;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.statistic.Statistic;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;

public class MockPassageRepo extends AbstractMockRepository implements PassageRepo {

	Logger log = Logger.getLogger(MockPassageRepo.class);
	private Map<Long, Passage> passages = new HashMap<Long, Passage>();

	@Override
	public long createPassage(Passage passage) {
		if (passage.getId() > 0) {
			throw new IllegalArgumentException("An object with that id already exists in the database");
		}

		passage.setId(getNextId());
		passages.put(passage.getId(), passage);
		log.debug("MOCK: Created passage in db: " + passage);
		return passage.getId();
	}

	@Override
	public void updatePassage(Passage passage) {
		if (passage.getId() < 1) {
			throw new IllegalArgumentException("Object has no ID");
		}

		passages.put(passage.getId(), passage);
		log.debug("MOCK: Updated passage in db: " + passage);
	}

	@Override
	public Passage getPassage(long id) {
		log.debug("MOCK: Got passage from db: " + passages.get(id));
		return passages.get(id);
	}

	@Override
	public Collection<Passage> getAllPassages() {
		log.debug("MOCK: Got all passages from db");
		return passages.values();
	}

	@Override
	public Passage removePassage(long id) {
		log.debug("MOCK: Removed passage from db: " + passages.get(id));
		return passages.remove(id);

	}

	@Override
	public void removeAllPassages() {
		passages.clear();
		log.debug("MOCK: Removed all passages from db");
	}

	@Override
	public Collection<Passage> getPassagesByVehicle(long id) {
		Set<Passage> passagesByVehicle = new HashSet<Passage>();

		for (Entry<Long, Passage> entry : passages.entrySet()) {
			if (entry.getValue().getVehicle().getId() == id) {
				passagesByVehicle.add(entry.getValue());
			}
		}
		log.debug("MOCK: Got passages by vehicle with id " + id + " from db");
		return passagesByVehicle;
	}

	@Override
	public Collection<Passage> getPassagesByStation(long id) {
		Set<Passage> passagesByStation = new HashSet<Passage>();

		for (Entry<Long, Passage> entry : passages.entrySet()) {
			if (entry.getValue().getStation().getId() == id) {
				passagesByStation.add(entry.getValue());
			}
		}
		log.debug("MOCK: Got passages by station with id " + id + " from db");
		return passagesByStation;
	}

	@Override
	public Statistic getTotalStatistics() {
		int totRevenue = 0;
		for (Entry<Long, Passage> entry : passages.entrySet()) {
			totRevenue += entry.getValue().getFee();
		}
		int totPassages = passages.size();
		log.debug("MOCK: Got total statistics from db");
		return new Statistic((long) totRevenue, (long) totPassages);

	}

	@Override
	public Statistic getStatisticsByStation(Station station) {
		int stationRevenue = 0;
		for (Passage passage : getPassagesByStation(station.getId())) {
			stationRevenue += passage.getFee();
		}
		int stationPassages = getPassagesByStation(station.getId()).size();
		log.debug("MOCK: Got statistics by station with id: " + station.getId() + " from db");
		return new Statistic((long) stationRevenue, (long) stationPassages);

	}

	@Override
	public Statistic getStatisticsByVehicle(Vehicle vehicle) {
		int vehicleRevenue = 0;
		for (Passage passage : getPassagesByVehicle(vehicle.getId())) {
			vehicleRevenue += passage.getFee();
		}
		int vehiclePassages = getPassagesByVehicle(vehicle.getId()).size();
		log.debug("MOCK: Got statistics by vehicle with id: " + vehicle.getId() + " from db");
		return new Statistic((long) vehicleRevenue, (long) vehiclePassages);

	}
}
