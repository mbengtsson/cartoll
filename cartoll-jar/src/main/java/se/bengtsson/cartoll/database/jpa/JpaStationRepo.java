package se.bengtsson.cartoll.database.jpa;

import java.util.Collection;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import se.bengtsson.cartoll.database.StationRepo;
import se.bengtsson.cartoll.domain.station.Station;

public class JpaStationRepo extends AbstractJpaRepository implements StationRepo {

	Logger log = Logger.getLogger(JpaStationRepo.class);

	@Override
	@Transactional
	public long createStation(Station station) {
		em.persist(station);
		log.debug("JPA: Created station in db: " + station);
		return station.getId();
	}

	@Override
	@Transactional
	public void updateStation(Station station) {
		em.merge(station);
		log.debug("JPA: Updated station in db: " + station);

	}

	@Override
	public Station getStation(long id) {
		Station station = em.find(Station.class, id);
		log.debug("JPA: Got station from db: " + station);
		return station;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Station> getAllStations() {
		Query query = em.createNamedQuery("getAllStations");
		log.debug("JPA: Got all stations from db");
		return query.getResultList();
	}

	@Override
	@Transactional
	public Station removeStation(long id) {
		Station station = getStation(id);
		em.remove(station);
		log.debug("JPA: Removed station from db: " + station);
		return station;
	}

	@Override
	@Transactional
	public void removeAllStations() {
		Query query = em.createNamedQuery("removeAllStations");
		query.executeUpdate();
		log.debug("JPA: Removed all stations from db");
	}

}
