package se.bengtsson.cartoll.database;

import java.util.Collection;

import se.bengtsson.cartoll.domain.owner.Owner;

public interface OwnerRepo {

	public long createOwner(Owner owner);

	public void updateOwner(Owner owner);

	public Owner getOwner(long id);

	public Collection<Owner> getAllOwners();

	public Owner removeOwner(long id);

	public void removeAllOwners();

}
