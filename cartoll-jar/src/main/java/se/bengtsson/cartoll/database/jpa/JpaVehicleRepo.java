package se.bengtsson.cartoll.database.jpa;

import java.util.Collection;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import se.bengtsson.cartoll.database.VehicleRepo;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;

public class JpaVehicleRepo extends AbstractJpaRepository implements VehicleRepo {

	Logger log = Logger.getLogger(JpaVehicleRepo.class);

	@Override
	@Transactional
	public long createVehicle(Vehicle vehicle) {
		em.persist(vehicle);
		log.debug("JPA: Created vehicle in db: " + vehicle);
		return vehicle.getId();
	}

	@Override
	@Transactional
	public void updateVehicle(Vehicle vehicle) {
		em.merge(vehicle);
		log.debug("JPA: Updated vehicle in db: " + vehicle);
	}

	@Override
	public Vehicle getVehicle(long id) {
		Vehicle vehicle = em.find(Vehicle.class, id);
		log.debug("JPA: Got vehicle from db: " + vehicle);
		return vehicle;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Vehicle> getAllVehicles() {
		Query query = em.createNamedQuery("getAllVehicles");
		log.debug("JPA: Got all vehicle from db");
		return query.getResultList();
	}

	@Override
	@Transactional
	public Vehicle removeVehicle(long id) {
		Vehicle vehicle = getVehicle(id);
		em.remove(vehicle);
		log.debug("JPA: Removed vehicle from db: " + vehicle);
		return vehicle;
	}

	@Override
	@Transactional
	public void removeAllVehicles() {
		Query query = em.createNamedQuery("removeAllVehicles");
		query.executeUpdate();
		log.debug("JPA: Removed all vehicles from db");
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Vehicle> getVehiclesByOwner(long id) {
		Query query = em.createNamedQuery("getVehiclesByOwner");
		query.setParameter("id", id);
		log.debug("JPA: Got vehicles by owner with id " + id + " from db");
		return query.getResultList();
	}

}
