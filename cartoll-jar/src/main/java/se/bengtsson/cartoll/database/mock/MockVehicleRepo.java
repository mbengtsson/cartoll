package se.bengtsson.cartoll.database.mock;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.log4j.Logger;

import se.bengtsson.cartoll.database.VehicleRepo;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;

public class MockVehicleRepo extends AbstractMockRepository implements VehicleRepo {

	Logger log = Logger.getLogger(MockVehicleRepo.class);
	private Map<Long, Vehicle> vehicles = new HashMap<Long, Vehicle>();

	@Override
	public long createVehicle(Vehicle vehicle) {
		if (vehicle.getId() > 0) {
			throw new IllegalArgumentException("An object with that id already exists in the database");
		}

		vehicle.setId(getNextId());
		vehicles.put(vehicle.getId(), vehicle);
		log.debug("MOCK: Created vehicle in db: " + vehicle);
		return vehicle.getId();
	}

	@Override
	public void updateVehicle(Vehicle vehicle) {
		if (vehicle.getId() < 1) {
			throw new IllegalArgumentException("Object has no ID");
		}

		vehicles.put(vehicle.getId(), vehicle);
		log.debug("MOCK: Updated vehicle in db: " + vehicle);
	}

	@Override
	public Vehicle getVehicle(long id) {
		log.debug("MOCK: Got vehicle from db: " + vehicles.get(id));
		return vehicles.get(id);
	}

	@Override
	public Collection<Vehicle> getAllVehicles() {
		log.debug("MOCK: Got all vehicles from db");
		return vehicles.values();
	}

	@Override
	public Vehicle removeVehicle(long id) {
		log.debug("MOCK: Removed vehicle from db: " + vehicles.get(id));
		return vehicles.remove(id);
	}

	@Override
	public void removeAllVehicles() {
		vehicles.clear();
		log.debug("MOCK: Removed all vehicles from db");
	}

	@Override
	public Collection<Vehicle> getVehiclesByOwner(long id) {
		Set<Vehicle> vehiclesByOwner = new HashSet<Vehicle>();

		for (Entry<Long, Vehicle> entry : vehicles.entrySet()) {
			if (entry.getValue().getOwner().getId() == id) {
				vehiclesByOwner.add(entry.getValue());
			}
		}
		log.debug("MOCK: Got vehicles by owner with id " + id + " from db");
		return vehiclesByOwner;
	}

}
