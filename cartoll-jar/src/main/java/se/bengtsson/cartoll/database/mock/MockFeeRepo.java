package se.bengtsson.cartoll.database.mock;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import se.bengtsson.cartoll.database.FeeRepo;
import se.bengtsson.cartoll.domain.fee.Fee;

public class MockFeeRepo extends AbstractMockRepository implements FeeRepo {

	Logger log = Logger.getLogger(MockFeeRepo.class);
	private Map<String, Fee> fees = new HashMap<String, Fee>();

	public MockFeeRepo() {
		createFee(new Fee("car", 18, 13, 8, 0, 0));
		createFee(new Fee("taxi", 20, 15, 10, 0, 5));
		createFee(new Fee("truck", 25, 20, 12, 5, 10));
	}

	@Override
	public String createFee(Fee fee) {

		fees.put(fee.getType(), fee);
		log.debug("MOCK: Created fee in db: " + fee);
		return fee.getType();
	}

	@Override
	public void updateFee(Fee fee) {

		fees.put(fee.getType(), fee);
		log.debug("MOCK: Updated fee in db: " + fee);

	}

	@Override
	public Fee getFee(String type) {
		log.debug("MOCK: Got fee from db: " + fees.get(type));
		return fees.get(type);
	}

	@Override
	public Collection<Fee> getAllFees() {
		log.debug("MOCK: Got all fees from db");
		return fees.values();
	}

	@Override
	public Fee removeFee(String type) {
		log.debug("MOCK: Removed fee from db: " + fees.get(type));
		return fees.remove(type);
	}

	@Override
	public void removeAllFees() {
		fees.clear();
		log.debug("MOCK: Removed all fees from db");
	}

}
