package se.bengtsson.cartoll.database.mock;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import se.bengtsson.cartoll.database.StationRepo;
import se.bengtsson.cartoll.domain.station.Station;

public class MockStationRepo extends AbstractMockRepository implements StationRepo {

	Logger log = Logger.getLogger(MockStationRepo.class);
	private Map<Long, Station> stations = new HashMap<Long, Station>();

	@Override
	public long createStation(Station station) {
		if (station.getId() > 0) {
			throw new IllegalArgumentException("An object with that id already exists in the database");
		}

		station.setId(getNextId());
		stations.put(station.getId(), station);
		log.debug("MOCK: Created station in db: " + station);
		return station.getId();
	}

	@Override
	public void updateStation(Station station) {
		if (station.getId() < 1) {
			throw new IllegalArgumentException("Object has no ID");
		}

		stations.put(station.getId(), station);
		log.debug("MOCK: Updated station in db: " + station);
	}

	@Override
	public Station getStation(long id) {
		log.debug("MOCK: Got passage from db: " + stations.get(id));
		return stations.get(id);
	}

	@Override
	public Collection<Station> getAllStations() {
		log.debug("MOCK: Got all stations from db");
		return stations.values();
	}

	@Override
	public Station removeStation(long id) {
		log.debug("MOCK: Removed station from db: " + stations.get(id));
		return stations.remove(id);

	}

	@Override
	public void removeAllStations() {
		stations.clear();
		log.debug("MOCK: Removed all stations from db");
	}

}
