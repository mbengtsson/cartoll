package se.bengtsson.cartoll.database.jpa;

import java.util.Collection;

import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import se.bengtsson.cartoll.database.OwnerRepo;
import se.bengtsson.cartoll.domain.owner.Owner;

public class JpaOwnerRepo extends AbstractJpaRepository implements OwnerRepo {

	Logger log = Logger.getLogger(JpaOwnerRepo.class);

	@Override
	@Transactional
	public long createOwner(Owner owner) {
		em.persist(owner);
		log.debug("JPA: Created owner in db: " + owner);
		return owner.getId();
	}

	@Override
	@Transactional
	public void updateOwner(Owner owner) {
		em.merge(owner);
		log.debug("JPA: Updated owner in db: " + owner);
	}

	@Override
	public Owner getOwner(long id) {
		Owner owner = em.find(Owner.class, id);
		log.debug("JPA: Got owner from db: " + owner);
		return owner;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Collection<Owner> getAllOwners() {
		Query query = em.createNamedQuery("getAllOwners");
		log.debug("JPA: Got all owners from db");
		return query.getResultList();
	}

	@Override
	@Transactional
	public Owner removeOwner(long id) {
		Owner owner = getOwner(id);
		em.remove(owner);
		log.debug("JPA: Removed owner from db: " + owner);
		return owner;
	}

	@Override
	@Transactional
	public void removeAllOwners() {
		Query query = em.createNamedQuery("removeAllOwners");
		query.executeUpdate();
		log.debug("JPA: Removed all owners from db");
	}

}
