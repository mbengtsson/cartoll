package se.bengtsson.cartoll.database.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractJpaRepository {

	@PersistenceContext
	EntityManager em;
}
