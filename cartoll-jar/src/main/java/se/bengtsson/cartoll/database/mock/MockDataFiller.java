package se.bengtsson.cartoll.database.mock;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.vehicle.type.Car;
import se.bengtsson.cartoll.domain.vehicle.type.Taxi;
import se.bengtsson.cartoll.domain.vehicle.type.Truck;
import se.bengtsson.cartoll.services.OwnerService;
import se.bengtsson.cartoll.services.StationService;
import se.bengtsson.cartoll.services.VehicleService;

@Component
public class MockDataFiller {

	boolean initilized = false;

	@Autowired
	private OwnerService ownerService;
	@Autowired
	private VehicleService vehicleService;
	@Autowired
	private StationService stationService;

	public void initilizeMockDataIfNecessary() {
		if (!initilized) {
			initilizeMockData();
			initilized = true;
		}
	}

	private void initilizeMockData() {
		Owner owner1 =
				ownerService
						.createOwner(new Owner("123456-7890", "John", "Doe", "Random road 2", "123 45", "Sometown"));
		Owner owner2 =
				ownerService
						.createOwner(new Owner("147258-3690", "James", "Smith", "Somestreet 8", "234 98", "Anyton"));

		vehicleService.createVehicle(new Car("CAR-123", owner1));
		vehicleService.createVehicle(new Taxi("CAB-123", owner2, true));
		vehicleService.createVehicle(new Taxi("CAB-234", owner1, false));
		vehicleService.createVehicle(new Truck("TRC-123", owner2, 5.7f));
		vehicleService.createVehicle(new Truck("TRC-234", owner1, 9.7f));

		stationService.createStation(new Station("Test station 1"));
		stationService.createStation(new Station("Test station 2"));
	}
}
