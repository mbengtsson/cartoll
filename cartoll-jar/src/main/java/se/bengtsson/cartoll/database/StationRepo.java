package se.bengtsson.cartoll.database;

import java.util.Collection;

import se.bengtsson.cartoll.domain.station.Station;

public interface StationRepo {

	public long createStation(Station station);

	public void updateStation(Station station);

	public Station getStation(long id);

	public Collection<Station> getAllStations();

	public Station removeStation(long id);

	public void removeAllStations();

}
