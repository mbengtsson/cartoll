package se.bengtsson.cartoll.database.mock;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import se.bengtsson.cartoll.database.OwnerRepo;
import se.bengtsson.cartoll.domain.owner.Owner;

public class MockOwnerRepo extends AbstractMockRepository implements OwnerRepo {

	Logger log = Logger.getLogger(MockOwnerRepo.class);
	private Map<Long, Owner> owners = new HashMap<Long, Owner>();

	@Override
	public long createOwner(Owner owner) {
		if (owner.getId() > 0) {
			throw new IllegalArgumentException("An object with that id already exists in the database");
		}

		owner.setId(getNextId());
		owners.put(owner.getId(), owner);
		log.debug("MOCK: Created owner in db: " + owner);
		return owner.getId();
	}

	@Override
	public void updateOwner(Owner owner) {
		if (owner.getId() < 1) {
			throw new IllegalArgumentException("Object has no ID");
		}

		owners.put(owner.getId(), owner);
		log.debug("MOCK: Updated owner in db: " + owner);
	}

	@Override
	public Owner getOwner(long id) {
		log.debug("MOCK: Got owner from db: " + owners.get(id));
		return owners.get(id);
	}

	@Override
	public Collection<Owner> getAllOwners() {
		log.debug("MOCK: Got all owners from db");
		return owners.values();
	}

	@Override
	public Owner removeOwner(long id) {
		log.debug("MOCK: Removed owner from db: " + owners.get(id));
		return owners.remove(id);

	}

	@Override
	public void removeAllOwners() {
		owners.clear();
		log.debug("MOCK: Removed all owners from db");
	}

}
