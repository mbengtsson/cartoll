package se.bengtsson.cartoll.services.implementation;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.bengtsson.cartoll.database.FeeRepo;
import se.bengtsson.cartoll.domain.fee.Fee;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.vehicle.type.Car;
import se.bengtsson.cartoll.domain.vehicle.type.Taxi;
import se.bengtsson.cartoll.domain.vehicle.type.Truck;
import se.bengtsson.cartoll.services.FeeService;
import se.bengtsson.cartoll.services.feestrategy.CarStrategy;
import se.bengtsson.cartoll.services.feestrategy.FeeStrategy;
import se.bengtsson.cartoll.services.feestrategy.TaxiStrategy;
import se.bengtsson.cartoll.services.feestrategy.TruckStrategy;

@Service
public class FeeServiceImpl implements FeeService {

	@Autowired
	private FeeRepo feeRepo;

	@Override
	public Fee createFee(Fee fee) {
		String type = feeRepo.createFee(fee);
		return getFee(type);
	}

	@Override
	public void updateFee(Fee fee) {
		feeRepo.updateFee(fee);
	}

	@Override
	public Fee getFee(String type) {
		return feeRepo.getFee(type);
	}

	@Override
	public Collection<Fee> getAllFees() {
		return feeRepo.getAllFees();
	}

	@Override
	public Fee removeFee(String type) {
		return feeRepo.removeFee(type);
	}

	@Override
	public void removeAllFees() {
		feeRepo.removeAllFees();
	}

	/**
	 * This method uses a strategy-pattern to calculate the fee of a passage
	 * based on the vehicle-type of the passage. Throws an
	 * IllegalArgumentException if there is a unimplemented vehicle type
	 * 
	 * @param passage
	 * @return the fee for the passage
	 */
	@Override
	public int getFeeForPassage(Passage passage) {
		FeeStrategy feeStrategy = null;

		if (passage.getVehicle() instanceof Car) {
			feeStrategy = new CarStrategy(passage, getFee("car"));
		} else if (passage.getVehicle() instanceof Taxi) {
			feeStrategy = new TaxiStrategy(passage, getFee("taxi"));
		} else if (passage.getVehicle() instanceof Truck) {
			feeStrategy = new TruckStrategy(passage, getFee("truck"));
		} else {
			throw new IllegalArgumentException(String.format("Passage: %s contains illegal vehicle type", passage));
		}

		return feeStrategy.calculateFee();
	}

	@Override
	public void setFeeRepo(FeeRepo feeRepo) {
		this.feeRepo = feeRepo;

	}

}
