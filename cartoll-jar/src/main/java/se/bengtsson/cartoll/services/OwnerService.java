package se.bengtsson.cartoll.services;

import java.util.Collection;

import se.bengtsson.cartoll.database.OwnerRepo;
import se.bengtsson.cartoll.domain.owner.Owner;

public interface OwnerService {

	public Owner createOwner(Owner owner);

	public void updateOwner(Owner owner);

	public Owner getOwner(long id);

	public Collection<Owner> getAllOwners();

	public Owner removeOwner(long id);

	public void removeAllOwners();

	public void setOwnerRepo(OwnerRepo ownerRepo);

}
