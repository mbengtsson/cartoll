package se.bengtsson.cartoll.services.feestrategy;

import se.bengtsson.cartoll.domain.fee.Fee;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.vehicle.type.Truck;

/**
 * Strategy for calculating the fee of a truck
 * 
 * @author Marcus Bengtsson
 * 
 */
public class TruckStrategy extends FeeStrategy {

	private Truck truck;
	private int weightFee;
	private final int WEIGHT_LIMIT = 6;

	public TruckStrategy(Passage passage, Fee fee) {
		super(passage, fee);
		this.truck = (Truck) passage.getVehicle();
		this.weightFee = fee.getSpecialFee();
	}

	@Override
	protected int getHighRangeFee() {
		return addWeightFee(fee.getHighFee());
	}

	@Override
	protected int getMediumRangeFee() {
		return addWeightFee(fee.getMediumFee());
	}

	@Override
	protected int getLowRangeFee() {
		return addWeightFee(fee.getLowFee());
	}

	@Override
	protected int getFreeRangeFee() {
		return addWeightFee(fee.getFreeFee());
	}

	/**
	 * Adds overweight fee to the base fee of a truck if the trucks weight
	 * exceeds the weight-limit
	 * 
	 * @param base
	 *            fee
	 * @return fee with added overweight fee
	 */
	private int addWeightFee(int fee) {
		int overWeight = (int) (truck.getWeight() - WEIGHT_LIMIT);

		if (overWeight > 0) {
			fee += overWeight * weightFee;
		}

		return fee;
	}

}
