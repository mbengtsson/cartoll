package se.bengtsson.cartoll.services.implementation;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.bengtsson.cartoll.database.OwnerRepo;
import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.services.OwnerService;

@Service
public class OwnerServiceImpl implements OwnerService {

	@Autowired
	private OwnerRepo ownerRepo;

	@Override
	public Owner createOwner(Owner owner) {
		long id = ownerRepo.createOwner(owner);
		return getOwner(id);
	}

	@Override
	public void updateOwner(Owner owner) {
		ownerRepo.updateOwner(owner);
	}

	@Override
	public Owner getOwner(long id) {
		return ownerRepo.getOwner(id);
	}

	@Override
	public Collection<Owner> getAllOwners() {
		return ownerRepo.getAllOwners();
	}

	@Override
	public Owner removeOwner(long id) {
		return ownerRepo.removeOwner(id);

	}

	@Override
	public void removeAllOwners() {
		ownerRepo.removeAllOwners();
	}

	@Override
	public void setOwnerRepo(OwnerRepo ownerRepo) {
		this.ownerRepo = ownerRepo;

	}

}
