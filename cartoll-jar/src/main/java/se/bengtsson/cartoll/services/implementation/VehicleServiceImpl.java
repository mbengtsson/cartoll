package se.bengtsson.cartoll.services.implementation;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.bengtsson.cartoll.database.VehicleRepo;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;
import se.bengtsson.cartoll.services.VehicleService;

@Service
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	private VehicleRepo vehicleRepo;

	@Override
	public Vehicle createVehicle(Vehicle vehicle) {
		long id = vehicleRepo.createVehicle(vehicle);
		return getVehicle(id);
	}

	@Override
	public void updateVehicle(Vehicle vehicle) {
		vehicleRepo.updateVehicle(vehicle);
	}

	@Override
	public Vehicle getVehicle(long id) {
		return vehicleRepo.getVehicle(id);
	}

	@Override
	public Collection<Vehicle> getAllVehicles() {

		return vehicleRepo.getAllVehicles();
	}

	@Override
	public Vehicle removeVehicle(long id) {
		return vehicleRepo.removeVehicle(id);

	}

	@Override
	public void removeAllVehicles() {
		vehicleRepo.removeAllVehicles();
	}

	@Override
	public Collection<Vehicle> getVehiclesByOwner(long id) {
		return vehicleRepo.getVehiclesByOwner(id);
	}

	@Override
	public void setVehicleRepo(VehicleRepo vehicleRepo) {
		this.vehicleRepo = vehicleRepo;
	}

}
