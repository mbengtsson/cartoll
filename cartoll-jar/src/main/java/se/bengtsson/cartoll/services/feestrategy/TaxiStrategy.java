package se.bengtsson.cartoll.services.feestrategy;

import se.bengtsson.cartoll.domain.fee.Fee;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.vehicle.type.Taxi;

/**
 * Strategy for calculating the fee of a taxi
 * 
 * @author Marcus Bengtsson
 * 
 */
public class TaxiStrategy extends FeeStrategy {

	private Taxi taxi;
	private int envFee;

	public TaxiStrategy(Passage passage, Fee fee) {
		super(passage, fee);
		this.taxi = (Taxi) passage.getVehicle();
		this.envFee = fee.getSpecialFee();
	}

	@Override
	protected int getHighRangeFee() {
		return addEnvFee(fee.getHighFee());
	}

	@Override
	protected int getMediumRangeFee() {
		return addEnvFee(fee.getMediumFee());
	}

	@Override
	protected int getLowRangeFee() {
		return addEnvFee(fee.getLowFee());
	}

	@Override
	protected int getFreeRangeFee() {
		return fee.getFreeFee();
	}

	/**
	 * Adds enviromental fee to the base fee of a taxi if the taxi isn't
	 * enviromental certificated
	 * 
	 * @param base
	 *            fee
	 * @return fee with added enviromental fee
	 */
	private int addEnvFee(int fee) {
		if (!taxi.isEnvCertificated()) {
			fee += envFee;
		}

		return fee;
	}

}
