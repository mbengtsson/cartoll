package se.bengtsson.cartoll.services;

import java.util.Collection;

import se.bengtsson.cartoll.database.VehicleRepo;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;

public interface VehicleService {

	public Vehicle createVehicle(Vehicle vehicle);

	public void updateVehicle(Vehicle vehicle);

	public Vehicle getVehicle(long id);

	public Collection<Vehicle> getAllVehicles();

	public Vehicle removeVehicle(long id);

	public void removeAllVehicles();

	public Collection<Vehicle> getVehiclesByOwner(long id);

	public void setVehicleRepo(VehicleRepo vehicleRepo);

}
