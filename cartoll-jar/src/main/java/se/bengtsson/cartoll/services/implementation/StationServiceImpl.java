package se.bengtsson.cartoll.services.implementation;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.bengtsson.cartoll.database.StationRepo;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.services.StationService;

@Service
public class StationServiceImpl implements StationService {

	@Autowired
	private StationRepo stationRepo;

	@Override
	public Station createStation(Station station) {
		long id = stationRepo.createStation(station);
		return getStation(id);
	}

	@Override
	public void updateStation(Station station) {
		stationRepo.updateStation(station);
	}

	@Override
	public Station getStation(long id) {
		return stationRepo.getStation(id);
	}

	@Override
	public Collection<Station> getAllStations() {
		return stationRepo.getAllStations();
	}

	@Override
	public Station removeStation(long id) {
		return stationRepo.removeStation(id);

	}

	@Override
	public void removeAllStations() {
		stationRepo.removeAllStations();
	}

	@Override
	public void setStationRepo(StationRepo stationRepo) {
		this.stationRepo = stationRepo;

	}

}
