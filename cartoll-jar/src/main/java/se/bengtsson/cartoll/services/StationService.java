package se.bengtsson.cartoll.services;

import java.util.Collection;

import se.bengtsson.cartoll.database.StationRepo;
import se.bengtsson.cartoll.domain.station.Station;

public interface StationService {

	public Station createStation(Station station);

	public void updateStation(Station station);

	public Station getStation(long id);

	public Collection<Station> getAllStations();

	public Station removeStation(long id);

	public void removeAllStations();

	public void setStationRepo(StationRepo stationRepo);

}
