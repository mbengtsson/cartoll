package se.bengtsson.cartoll.services.implementation;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import se.bengtsson.cartoll.database.PassageRepo;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.statistic.Statistic;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;
import se.bengtsson.cartoll.services.FeeService;
import se.bengtsson.cartoll.services.PassageService;

@Service
public class PassageServiceImpl implements PassageService {

	@Autowired
	private PassageRepo passageRepo;
	@Autowired
	private FeeService feeService;

	@Override
	public Passage createPassage(Passage passage) {
		passage.setFee(feeService.getFeeForPassage(passage));
		long id = passageRepo.createPassage(passage);
		return getPassage(id);
	}

	@Override
	public void updatePassage(Passage passage) {
		passageRepo.updatePassage(passage);
	}

	@Override
	public Passage getPassage(long id) {
		return passageRepo.getPassage(id);
	}

	@Override
	public Collection<Passage> getAllPassages() {
		return passageRepo.getAllPassages();
	}

	@Override
	public Passage removePassage(long id) {
		return passageRepo.removePassage(id);

	}

	@Override
	public void removeAllPassages() {
		passageRepo.removeAllPassages();

	}

	@Override
	public Collection<Passage> getPassagesByVehicle(long id) {
		return passageRepo.getPassagesByVehicle(id);
	}

	@Override
	public Collection<Passage> getPassagesByStation(long id) {
		return passageRepo.getPassagesByStation(id);
	}

	@Override
	public Statistic getTotalStatistics() {
		return passageRepo.getTotalStatistics();
	}

	@Override
	public Statistic getStatisticsByStation(Station station) {
		return passageRepo.getStatisticsByStation(station);
	}

	@Override
	public Statistic getStatisticsByVehicle(Vehicle vehicle) {
		return passageRepo.getStatisticsByVehicle(vehicle);
	}

	@Override
	public void setPassageRepo(PassageRepo passageRepo) {
		this.passageRepo = passageRepo;

	}

	@Override
	public void setFeeService(FeeService feeService) {
		this.feeService = feeService;

	}
}
