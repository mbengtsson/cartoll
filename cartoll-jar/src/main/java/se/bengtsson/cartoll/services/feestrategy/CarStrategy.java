package se.bengtsson.cartoll.services.feestrategy;

import se.bengtsson.cartoll.domain.fee.Fee;
import se.bengtsson.cartoll.domain.passage.Passage;

/**
 * Strategy for calculating the fee of a car
 * 
 * @author Marcus Bengtsson
 * 
 */
public class CarStrategy extends FeeStrategy {

	public CarStrategy(Passage passage, Fee fee) {
		super(passage, fee);
	}

	@Override
	protected int getHighRangeFee() {
		return fee.getHighFee();
	}

	@Override
	protected int getMediumRangeFee() {
		return fee.getMediumFee();
	}

	@Override
	protected int getLowRangeFee() {
		return fee.getLowFee();
	}

	@Override
	protected int getFreeRangeFee() {
		return fee.getFreeFee();
	}
}
