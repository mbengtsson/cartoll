package se.bengtsson.cartoll.services;

import java.util.Collection;

import se.bengtsson.cartoll.database.FeeRepo;
import se.bengtsson.cartoll.domain.fee.Fee;
import se.bengtsson.cartoll.domain.passage.Passage;

public interface FeeService {

	public Fee createFee(Fee fee);

	public void updateFee(Fee fee);

	public Fee getFee(String type);

	public Collection<Fee> getAllFees();

	public Fee removeFee(String type);

	public void removeAllFees();

	public int getFeeForPassage(Passage passage);

	public void setFeeRepo(FeeRepo feeRepo);

}
