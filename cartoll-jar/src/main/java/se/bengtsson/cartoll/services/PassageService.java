package se.bengtsson.cartoll.services;

import java.util.Collection;

import se.bengtsson.cartoll.database.PassageRepo;
import se.bengtsson.cartoll.domain.passage.Passage;
import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.statistic.Statistic;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;

public interface PassageService {

	public Passage createPassage(Passage passage);

	public void updatePassage(Passage passage);

	public Passage getPassage(long id);

	public Collection<Passage> getAllPassages();

	public Passage removePassage(long id);

	public void removeAllPassages();

	public Collection<Passage> getPassagesByVehicle(long id);

	public Collection<Passage> getPassagesByStation(long id);

	public Statistic getTotalStatistics();

	public Statistic getStatisticsByStation(Station station);

	public Statistic getStatisticsByVehicle(Vehicle vehicle);

	public void setPassageRepo(PassageRepo passageRepo);

	public void setFeeService(FeeService feeService);
}
