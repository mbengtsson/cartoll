package se.bengtsson.cartoll.services.feestrategy;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.base.BaseDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.bengtsson.cartoll.domain.fee.Fee;
import se.bengtsson.cartoll.domain.passage.Passage;

/**
 * Abstract class for a strategy-pattern used to calculate the fee of a passage,
 * strategies for differnt vehicle-types inherit this class. This is an abstract
 * class instead of an interface to counter duplication of code in the different
 * strategies.
 * 
 * @author Marcus Bengtsson
 * 
 */

public abstract class FeeStrategy {

	private BaseDateTime time;
	protected Fee fee;

	public FeeStrategy(Passage passage, Fee fee) {
		time = passage.getTime();
		this.fee = fee;

	}

	/**
	 * 
	 * @return the fee based of the time of a passage
	 */
	public int calculateFee() {
		if (isInHighRange()) {
			return getHighRangeFee();

		} else if (isInMediumRange()) {
			return getMediumRangeFee();

		} else if (isInLowRange()) {
			return getLowRangeFee();

		} else {
			return getFreeRangeFee();
		}
	}

	/**
	 * 
	 * @return The high range fee for the vehicle in the passage
	 */
	protected abstract int getHighRangeFee();

	/**
	 * 
	 * @return The medium range fee for the vehicle in the passage
	 */
	protected abstract int getMediumRangeFee();

	/**
	 * 
	 * @return The low range fee for the vehicle in the passage
	 */
	protected abstract int getLowRangeFee();

	/**
	 * 
	 * @return The free range fee for the vehicle in the passage
	 */
	protected abstract int getFreeRangeFee();

	/**
	 * 
	 * @return true if time is in the high fee range
	 */
	private Boolean isInHighRange() {
		if (isTimeInRange("07:00", "08:00") || isTimeInRange("15:30", "17:00")) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @return true if time is in the medium fee range
	 */
	private Boolean isInMediumRange() {
		if (isTimeInRange("06:30", "07:00") || isTimeInRange("08:00", "08:30") || isTimeInRange("15:00", "15:30")
				|| isTimeInRange("17:00", "18:00")) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @return true if time is in the low fee range
	 */
	private Boolean isInLowRange() {
		if (isTimeInRange("06:00", "06:30") || isTimeInRange("08:30", "15:00") || isTimeInRange("18:00", "18:30")) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param start
	 *            time of the range represented as a string
	 * @param end
	 *            time of the range represented as a string
	 * @return true if time is in range
	 */
	private boolean isTimeInRange(String start, String end) {
		DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("HH:mm");
		DateTime startTime = dateTimeFormatter.parseDateTime(start);
		DateTime endTime = dateTimeFormatter.parseDateTime(end);

		DateTimeComparator dateTimeComparator = DateTimeComparator.getTimeOnlyInstance();
		if (dateTimeComparator.compare(time, startTime) >= 0 && dateTimeComparator.compare(time, endTime) < 0) {
			return true;
		} else {
			return false;
		}

	}

}
