package se.bengtsson.cartoll.domain.vehicle;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import se.bengtsson.cartoll.domain.owner.Owner;

/**
 * Object that represents a vehicle, vehicles will be charged a fee when passing
 * a station
 * 
 * @author Marcus Bengtsson
 * 
 */

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "vehicleType", discriminatorType = DiscriminatorType.STRING, length = 10)
@NamedQueries({ @NamedQuery(name = "getAllVehicles", query = "SELECT v FROM Vehicle v"),
		@NamedQuery(name = "removeAllVehicles", query = "DELETE FROM Vehicle"),
		@NamedQuery(name = "getVehiclesByOwner", query = "SELECT v FROM Vehicle v WHERE owner_id = :id")

})
public abstract class Vehicle {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String regNr;
	@ManyToOne()
	@JoinColumn(name = "owner_id")
	private Owner owner;

	public Vehicle() {

	}

	public Vehicle(String regNr, Owner owner) {
		setRegNr(regNr);
		setOwner(owner);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((regNr == null) ? 0 : regNr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vehicle other = (Vehicle) obj;
		if (id != other.id)
			return false;
		if (regNr == null) {
			if (other.regNr != null)
				return false;
		} else if (!regNr.equals(other.regNr))
			return false;
		return true;
	}

	public abstract String getNiceName();

	public abstract String getNiceType();

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public String getRegNr() {
		return regNr;
	}

	public void setRegNr(String regNr) {
		this.regNr = regNr;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
