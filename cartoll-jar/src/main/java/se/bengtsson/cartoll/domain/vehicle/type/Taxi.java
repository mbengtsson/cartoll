package se.bengtsson.cartoll.domain.vehicle.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;

/**
 * Object that represents a taxi, taxi inherit vehicles and will be charged a
 * fee when passing a station. Taxis may be enviromental certificated which will
 * affect the fee.
 * 
 * @author Marcus Bengtsson
 * 
 */
@Entity
@DiscriminatorValue(value = "taxi")
public class Taxi extends Vehicle {

	private boolean envCertificated;

	public Taxi() {

	}

	public Taxi(String regNr, Owner owner, boolean envCertificate) {
		super(regNr, owner);
		setEnvCertificated(envCertificate);

	}

	@Override
	public String toString() {
		return String.format("[%d, %s, %s, env-cert: %b]", getId(), getRegNr(), getOwner().getFullName(),
				isEnvCertificated());
	}

	@Override
	public String getNiceName() {
		String envCertificated = isEnvCertificated() ? "Miljöcertifierad" : "Ej miljöcertifierad";
		return String.format("%s - %s (%s)", getNiceType(), getRegNr(), envCertificated);
	}

	@Override
	public String getNiceType() {
		return "Taxi";
	}

	public boolean isEnvCertificated() {
		return envCertificated;
	}

	public void setEnvCertificated(boolean envCertificate) {
		this.envCertificated = envCertificate;
	}

}
