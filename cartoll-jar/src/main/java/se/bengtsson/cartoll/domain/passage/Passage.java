package se.bengtsson.cartoll.domain.passage;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.hibernate.annotations.Type;
import org.joda.time.base.BaseDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import se.bengtsson.cartoll.domain.station.Station;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;

/**
 * Object representing passage of a vehicle through a cartoll-station
 * 
 * @author Marcus Bengtsson
 * 
 */

@Entity
@NamedQueries({ @NamedQuery(name = "getAllPassages", query = "SELECT p FROM Passage p"),
		@NamedQuery(name = "removeAllPassages", query = "DELETE FROM Passage"),
		@NamedQuery(name = "getPassagesByVehicle", query = "SELECT p FROM Passage p WHERE vehicle_id = :id"),
		@NamedQuery(name = "getPassagesByStation", query = "SELECT p FROM Passage p WHERE station_id = :id") })
public class Passage {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@ManyToOne()
	@JoinColumn(name = "station_id")
	private Station station;
	@Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private BaseDateTime time;
	@ManyToOne()
	@JoinColumn(name = "vehicle_id")
	private Vehicle vehicle;
	private int fee;

	public Passage() {

	}

	public Passage(Station station, Vehicle vehicle, BaseDateTime time) {
		setStation(station);
		setVehicle(vehicle);
		setTime(time);
	}

	@Override
	public String toString() {
		DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm");
		return String.format("[%d, %s, %s, %s, %s, %skr]", getId(), timeFormatter.print(time), station.getName(),
				vehicle.getRegNr(), vehicle.getOwner().getFullName(), getFee());

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((station == null) ? 0 : station.hashCode());
		result = prime * result + ((time == null) ? 0 : time.hashCode());
		result = prime * result + ((vehicle == null) ? 0 : vehicle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Passage other = (Passage) obj;
		if (id != other.id)
			return false;
		if (station == null) {
			if (other.station != null)
				return false;
		} else if (!station.equals(other.station))
			return false;
		if (time == null) {
			if (other.time != null)
				return false;
		} else if (!time.equals(other.time))
			return false;
		if (vehicle == null) {
			if (other.vehicle != null)
				return false;
		} else if (!vehicle.equals(other.vehicle))
			return false;
		return true;
	}

	public String getNiceTime() {
		DateTimeFormatter timeFormatter = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm");
		return timeFormatter.print(time);
	}

	public long getId() {
		return id;
	}

	public Station getStation() {
		return station;
	}

	public BaseDateTime getTime() {
		return time;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public int getFee() {
		return fee;
	}

	public void setFee(int fee) {
		this.fee = fee;
	}

	public void setStation(Station station) {
		this.station = station;
	}

	public void setTime(BaseDateTime time) {
		this.time = time;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public void setId(long id) {
		this.id = id;
	}

}
