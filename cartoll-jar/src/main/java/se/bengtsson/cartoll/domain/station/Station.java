package se.bengtsson.cartoll.domain.station;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * Object representing a cartoll-station, when a vehicle passing the station a
 * fee will be charged
 * 
 * @author Marcus Bengtsson
 * 
 */

@Entity
@NamedQueries({ @NamedQuery(name = "getAllStations", query = "SELECT s FROM Station s"),
		@NamedQuery(name = "removeAllStations", query = "DELETE FROM Station") })
public class Station {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;

	public Station() {

	}

	public Station(String name) {
		setName(name);
	}

	@Override
	public String toString() {
		return String.format("[%d, %s]", getId(), getName());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Station other = (Station) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
