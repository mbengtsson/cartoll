package se.bengtsson.cartoll.domain.vehicle.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;

/**
 * Object that represents a car, cars inherit vehicles and will be charged a fee
 * when passing a station.
 * 
 * @author Marcus Bengtsson
 * 
 */
@Entity
@DiscriminatorValue(value = "car")
public class Car extends Vehicle {

	public Car() {

	}

	public Car(String regNr, Owner owner) {
		super(regNr, owner);
	}

	@Override
	public String toString() {
		return String.format("[%d, %s, %s]", getId(), getRegNr(), getOwner().getFullName());
	}

	@Override
	public String getNiceName() {
		return String.format("%s - %s", getNiceType(), getRegNr());
	}

	@Override
	public String getNiceType() {
		return "Bil";
	}

}
