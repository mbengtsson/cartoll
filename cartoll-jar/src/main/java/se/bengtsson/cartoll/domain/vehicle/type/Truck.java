package se.bengtsson.cartoll.domain.vehicle.type;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import se.bengtsson.cartoll.domain.owner.Owner;
import se.bengtsson.cartoll.domain.vehicle.Vehicle;

/**
 * Object that represents a truck, truck inherit vehicles and will be charged a
 * fee when passing a station. Trucks will be charged an extra fee if to heavy.
 * 
 * @author Marcus Bengtsson
 * 
 */
@Entity
@DiscriminatorValue(value = "truck")
public class Truck extends Vehicle {

	private double weight;

	public Truck() {

	}

	public Truck(String regNr, Owner owner, double Weight) {
		super(regNr, owner);
		setWeight(Weight);

	}

	@Override
	public String toString() {
		return String.format("[%d,  %s, %s, weight: %.2f]", getId(), getRegNr(), getOwner().getFullName(), getWeight());
	}

	@Override
	public String getNiceName() {
		return String.format("%s - %s (%.2f ton)", getNiceType(), getRegNr(), getWeight());
	}

	@Override
	public String getNiceType() {
		return "Lastbil";
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

}
