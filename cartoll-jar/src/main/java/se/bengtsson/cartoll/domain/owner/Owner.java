package se.bengtsson.cartoll.domain.owner;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * Object that represents the owner of a vehicle
 * 
 * @author Marcus Bengtsson
 * 
 */

@Entity
@NamedQueries({ @NamedQuery(name = "getAllOwners", query = "SELECT o FROM Owner o"),
		@NamedQuery(name = "removeAllOwners", query = "DELETE FROM Owner") })
public class Owner {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String socialSecurityNr;
	private String firstName;
	private String lastName;
	private String street;
	private String zipCode;
	private String city;

	public Owner() {

	}

	public Owner(String socialSecurityNr, String firstName, String lastName, String street, String zipCode, String city) {
		setSocialSecurityNr(socialSecurityNr);
		setFirstName(firstName);
		setLastName(lastName);
		setStreet(street);
		setZipCode(zipCode);
		setCity(city);
	}

	@Override
	public String toString() {
		return String.format("[%d, %s, %s, %s, %s, %s]", getId(), getSocialSecurityNr(), getFullName(), getStreet(),
				getZipCode(), getCity());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((socialSecurityNr == null) ? 0 : socialSecurityNr.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Owner other = (Owner) obj;
		if (id != other.id)
			return false;
		if (socialSecurityNr == null) {
			if (other.socialSecurityNr != null)
				return false;
		} else if (!socialSecurityNr.equals(other.socialSecurityNr))
			return false;
		return true;
	}

	public String getFullName() {
		return String.format("%s %s", firstName, lastName);
	}

	public String getSocialSecurityNr() {
		return socialSecurityNr;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getStreet() {
		return street;
	}

	public String getZipCode() {
		return zipCode;
	}

	public String getCity() {
		return city;
	}

	public void setSocialSecurityNr(String socialSecurityNr) {
		this.socialSecurityNr = socialSecurityNr;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public void setCity(String city) {
		this.city = city;
	}
}
