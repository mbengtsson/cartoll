package se.bengtsson.cartoll.domain.statistic;

/**
 * Object to keep statistics of passages.
 * 
 * @author Marcus Bengtsson
 * 
 */

public class Statistic {

	private long revenue;
	private long passages;

	public Statistic(Long revenue, long passages) {
		setRevenue(revenue == null ? 0 : revenue);
		setPassages(passages);
	}

	@Override
	public String toString() {
		return String.format("Revenue: %d, passages: %d", getRevenue(), getPassages());
	}

	public long getRevenue() {
		return revenue;
	}

	public long getPassages() {
		return passages;
	}

	public void setRevenue(long revenue) {
		this.revenue = revenue;
	}

	public void setPassages(long passages) {
		this.passages = passages;
	}

}
