package se.bengtsson.cartoll.domain.fee;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * Object that represents the fees for a vehicle passing a cartoll station
 * 
 * @author Marcus Bengtsson
 * 
 */

@Entity
@NamedQueries({ @NamedQuery(name = "removeAllFees", query = "DELETE FROM Fee"),
		@NamedQuery(name = "getAllFees", query = "SELECT f FROM Fee f") })
public class Fee {

	@Id
	private String type;
	private int highFee;
	private int mediumFee;
	private int lowFee;
	private int freeFee;
	private int specialFee;

	public Fee() {

	}

	public Fee(String type, int highFee, int mediumFee, int lowFee, int freeFee, int specialFee) {
		setType(type);
		setHighFee(highFee);
		setMediumFee(mediumFee);
		setLowFee(lowFee);
		setFreeFee(freeFee);
		setSpecialFee(specialFee);

	}

	@Override
	public String toString() {
		return String.format("[%s, %d, %d, %d, %d, %d]", type, highFee, mediumFee, lowFee, freeFee, specialFee);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fee other = (Fee) obj;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}

	public String getType() {
		return type;
	}

	public int getHighFee() {
		return highFee;
	}

	public int getMediumFee() {
		return mediumFee;
	}

	public int getLowFee() {
		return lowFee;
	}

	public int getFreeFee() {
		return freeFee;
	}

	public int getSpecialFee() {
		return specialFee;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setHighFee(int highFee) {
		this.highFee = highFee;
	}

	public void setMediumFee(int mediumFee) {
		this.mediumFee = mediumFee;
	}

	public void setLowFee(int lowFee) {
		this.lowFee = lowFee;
	}

	public void setFreeFee(int freeFee) {
		this.freeFee = freeFee;
	}

	public void setSpecialFee(int specialFee) {
		this.specialFee = specialFee;
	}

}
